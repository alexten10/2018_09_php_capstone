<?php


  /**
  *escaping fields for security
  *@param String $string - input field value
  */
//escape a string (result can be seen in source page)
function esc($string)
{
  return strip_tags(htmlspecialchars($string, ENT_QUOTES, 'UTF-8', false));
}



  /**
  *get information about user by typed email from database for loging in
  *@param String $dbh - database 'coffeeccino_db'
  *@param string $email_address - typed into email field string on login page
  */
function getUserByEmail($dbh, $email_address)
{
  //1. make query for database
  $query = "SELECT
            *
            FROM
            users
            WHERE
            email = :email";
  //2 prepare statment
  $stmt = $dbh->prepare($query);
  //3. bind (make connection between) id value to database stored id
  $stmt->bindValue(':email', $email_address, PDO:: PARAM_STR);
  //4. execute
  $stmt->execute();
  //5. return all found data
  return $stmt->fetch(PDO::FETCH_ASSOC);//NOT fetchAll, because returns info about only 1 user
  //if no user found the return result will be an empty array
}



  /**
  *get info about user using user_id
  *@param String $dbh - database 'coffeeccino_db'
  *@param string $id - user_id (taken from table users.user_id)
  */
function getUser($dbh, $id)
{
  //1. make query for database
  $query = "SELECT
            *
            FROM
            users
            WHERE
            user_id = :id";
  //2 prepare statment
  $stmt = $dbh->prepare($query);
  //3. bind (make connection between) id value to database stored id
  $stmt->bindValue(':id', $id, PDO:: PARAM_STR);
  //4. execute
  $stmt->execute();
  //5. return all found data
  return $stmt->fetch(PDO::FETCH_ASSOC);//NOT fetchAll, because returns info about only 1 user
  //if no user found the return result will be an empty array
}



/**
* Get list of categories for list menu
* @param $dbh PDO database handle
* @return Array result
*/
function getCategories($dbh) {

  $query = "SELECT
        DISTINCT #return only distinct (different) values, no duplicate values
        product_coffee.category
        FROM
        product_coffee
        ORDER BY
        category ASC";

  $stmt = $dbh->prepare($query);
  $stmt->execute();
  // fetch multiple results
  return $stmt->fetchAll(PDO::FETCH_ASSOC);
}



/**
* Get products array by category
* @param $dbh PDO database handle
* @param  string $category - name of category
* @return Array result
*/
function getProductsByCategory ($dbh, $category)
{
  $query = "SELECT *
            FROM product_coffee
            WHERE category = :category";
  
  $stmt = $dbh->prepare($query);
  $stmt->bindValue (':category', $category, PDO:: PARAM_STR);
  $stmt->execute();
  return $stmt->fetchAll(PDO::FETCH_ASSOC);
}



/**
* Get all products array
* @param $dbh PDO database handle
* @return Array result
*/
function getAllProducts ($dbh)
{
  $query = "SELECT *
            FROM product_coffee";
  
  $stmt = $dbh->prepare($query);
  $stmt->execute();
  return $stmt->fetchAll(PDO::FETCH_ASSOC);
}



/**
* Get random products for gefault shop_coffee.php page
* @param $dbh PDO database handle
* @param $limit Int Number of products
* @return Array result
*/
function getRandomProducts($dbh, $limit)
{
  $query = "SELECT *
            FROM product_coffee
            ORDER BY RAND() #random order products in array result
            LIMIT :limit";

  $stmt = $dbh->prepare($query);
  $stmt->bindValue(':limit', $limit, PDO::PARAM_INT);
  $stmt->execute();
  return $stmt->fetchAll(PDO::FETCH_ASSOC);
}



/**
* Get random products for gefault shop_coffee.php page
* @param $dbh PDO database handle
* @param $limit Int Number of products
* @return Array result
*/
function getProduct($dbh, $id)
{
  $query = "SELECT *
            FROM product_coffee
            WHERE product_id = :id
           ";

  $stmt = $dbh->prepare($query);
  $stmt->bindValue(':id', $id, PDO::PARAM_INT);
  $stmt->execute();
  return $stmt->fetch(PDO::FETCH_ASSOC);
}



//$query for live search, we need only product title and product_id which is used later
// to get detail info about a product
/**
* Get live search products 
* @param $dbh PDO database handle
* @param $limit Int Number of products
* @return Array result
*/
function getLiveSearchResults ($dbh, $keyword)
{
  $query = "SELECT 
            product_coffee.product_id, 
            product_coffee.title 
            FROM product_coffee 
            WHERE product_coffee.title 
            LIKE :keyword 
            LIMIT 4"; //limit results to max 4, search in title by $keyword (binded :keyword)
  $stmt = $dbh->prepare($query);
  $stmt->bindParam(':keyword',$keyword); //binding :keyword to the value of $keyword
  $stmt->execute();
  return $stmt->fetchAll(PDO::FETCH_ASSOC);
}



function search($dbh, $search)
{
  $query = "SELECT
            *
            FROM product_coffee 
            WHERE product_coffee.title
            LIKE :search";
            //WHERE
            //MATCH(book.title)
            //AGAINST(:search IN NATURAL LANGUAGE MODE)";  //for full word search (if type 'du' will search for exact 'du'

  $stmt = $dbh->prepare($query);
  $stmt->bindValue(':search', $search, PDO::PARAM_STR);
  $stmt->execute();
  return $stmt->fetchAll(PDO::FETCH_ASSOC);
}







