<footer> <!-- footer links goes here -->
  
  <div id="logo_bottom"><a href="index.php" title="Home"><img src="images/logo.png" alt="caffeeccino" /></a></div>

  <div id="footer_main"> <!-- ***** #footer_main start ***** -->
  
    <div id="footer_col1" class="column">
      <h4 class="header4" id="footer_col1_header">Coffeeccino</h4>
      <ul id="footer_list">
        <li><a href="shop_coffee.php" title="Shop Coffee">Shop coffee</a></li>
        <li><a href="about_us.php" title="About Us">About us</a></li>
        <li><a href="terms_of_use.php" title="Terms of Use">Terms of use</a></li>
        <li><a href="shipping_policy.php" title="Shipping Policy">Shipping policy</a></li>
        <li><a href="contact.php" title="Contact Us">Contact us</a></li>
      </ul> <!-- end #footer_list -->
    </div> <!-- end #footer_menu -->
    
    <div id="footer_col2" class="column">
      <h4 class="header4" id="footer_col2_header">Why coffee</h4>
      <p>
        Over the last several decades, coffee has been among the most heavily studied
        dietary components. And the news is mostly good. Moderate coffee consumption 
        (three to four cups per day) has been linked with longer lifespan. In fact,
        a November 2015 study in <i>Circulation</i> found that coffee consumption was associated
        with an 8% to 15% reduction in the risk of death (with larger reductions among
        those with higher coffee consumption).
      </p>
      <p>&copy; <i><b>Harvard Health Publications</b></i></p>

    </div> <!-- end #footer_col2 -->
    
    <div id="footer_col3" class="column">
      <h4 class="header4" id="footer_col3_header">Customer Care</h4>
      <p>Our customer’s satisfaction is very important to us!</p>
      <p>
        That’s why we love hearing from you on various social medias, through our website and by email, 
        because we know that all of your questions and comments have helped us be better at what we do.
      </p>
      <p><i><b>Sincerely yours, <br /> Coffeeccino team</b></i></p>
      
    </div> <!-- end #footer_col3 -->
    
  </div> <!-- ***** end #footer_main ***** -->
  


  <div id="copyright_socials"> <!-- ***** #copyright start ***** -->
    <div id="copyright"><p>&copy; Alex Ten 2018</p></div> <!-- end #copyright -->
    <div id="socials">
      <ul id="socials_list">
        <li><a href="https://www.facebook.com/" title="Facebook"><img src="images/Facebook_icon.svg" alt="Facebook" width="30" height="30" /></a></li>
        <li><a href="https://www.instagram.com/" title="Instagram"><img src="images/Instagram.svg" alt="Instagram" width="30" height="30" /></a></li>
        <li><a href="https://www.youtube.com/" title="YouTube"><img src="images/YouTube.svg" alt="YouTube" width="64" height="30" /></a></li>
      </ul>
    </div> <!-- end #socials -->
  </div> <!-- ***** end #copyright_socials ***** -->
  
</footer>