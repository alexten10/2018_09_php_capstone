<div id="header_nav"> <!-- ***** #header_nav start *****  -->

  <header> <!-- header start -->
    <div id="logo"><a href="index.php" title="Home"><img src="images/logo.png" alt="coffeeccino" /></a></div>
    <div id="tagline"><a href="shop_coffee.php" title="Shop Coffee">Delight in every drop</a></div>
    
    <div id="user_section"><!-- utility navigation(top right corner) -->
      <!-- utility navigation is conditional and depend on if a user logged in or not-->
      <?php if(!isset($_SESSION['logged_in'])) {
      echo '<span class="user_menu"><a href="registration.php">Register</a></span>';
      echo '<span class="user_menu"><a href="login.php">LogIn</a></span>';
      } else {
      echo '<span class="user_menu"><a href="logout.php">LogOut</a></span>';
      echo '<span class="user_menu"><a href="profile.php">Profile</a></span>';
      }
      ?>
    </div><!-- END #user_section-->
    
  </header> <!-- header end -->

  <!--[if LTE IE 8]>
    <h2>to see this website properly, please update your browser</h2>
  <![endif]-->

  <?php include '../includes/nav_menu.inc.php' ?>
  
</div><!-- ***** end header_nav ***** -->



