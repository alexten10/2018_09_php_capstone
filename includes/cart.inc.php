<div class="cart">
  <p>
    <small>You have <strong>1</strong> item in your cart</small><br />
    <small>
      <?php echo $_SESSION['cart']['title'] ?><br />
      $<?php echo $_SESSION['cart']['price'] ?>.
    </small><br />
    <small><a href="checkout.php">Checkout</a></small>
  </p>
</div>