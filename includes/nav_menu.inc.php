<nav> <!-- ***** navigation menu goes here ***** -->
  <div id="menu"> <!-- #menu start -->
  
    <!-- hamburger menu start-->
    <a href="#" id="menulink" title="Menu"><!-- need this link for iPad iPhone
                              compatibility with hamburger button -->
      <span id="hamburger_top"></span><!-- lines of the hamburger button -->
      <span id="hamburger_middle"></span>
      <span id="hamburger_bottom"></span>
    </a>
    <!-- hamburger menu end-->
    
    <!-- if the current page is active, php gives it class 'current', which 
         is styled by css file, to highlight current nav menu link -->
    <ul id="navlist">
      <li><a href="index.php"
             <?php if($active_page == 'index') {echo 'class="current"';}?>
             title="Home">Home</a></li>
      <li><a href="shop_coffee.php"
             <?php if($active_page == 'shop_coffee') {echo 'class="current"';}?>
             title="Shop Coffee" >Shop coffee</a></li>
      <li><a href="about_us.php"
             <?php if($active_page == 'about_us') {echo 'class="current"';}?>
             title="About Us">About Us</a></li>
      <li><a href="cart.php"
             <?php if($active_page == 'cart') {echo 'class="current"';}?>
             title="Cart">Cart</a></li>
      <li><a href="contact.php"
             <?php if($active_page == 'contact') {echo 'class="current"';}?>
             title="Contact">Contact Us</a></li>
    </ul>
    
  </div> <!-- end #menu -->
</nav> <!-- ***** end navigation ***** -->