<!doctype html>


<html lang="en">

  <head>
  
    <title><?php echo $title; ?></title>
    <meta charset="utf-8" />
    <meta name="viewport"
          content="width=device-width, initial-scale=1" />
          
    <link rel="shortcut icon" href="images/favicon64.png" type="image/png" /><!-- favorite icon in title link -->
    
    <!-- link to css file for desktops -->
    <link rel="stylesheet"
          type="text/css"
          href="styles/desktop.css"
          media="screen and (min-width: 768px)"
    />
    
    <!-- link to css file for mobiles -->
    <link rel="stylesheet"
          type="text/css"
          href="styles/mobile.css"
          media="screen and (max-width: 767px)"
    /> 
    
    <!-- CSS link for IE browser version 9 and less -->
    <!--[if LTE IE 9]>
          <link rel="stylesheet"
          type="text/css"
          href="styles/desktop.css"
          media="screen"
          />
    <![endif] --> 
    
    <!-- link to css file for printers -->
    <link rel="stylesheet"
          type="text/css"
          href="styles/print.css" 
          media="print" 
    />
    

    <?php if($active_page == 'cart') : ?>
      <!-- embedded css -->
      <style>
        .cart_h3{
          font-size: 26px;
          text-align: center;
        }

        .cart_paragraph{
          text-align: center;
        }
        
        .continue_shopping{
          text-align: center;
          margin-top: 50px;
        }
        
        #continue_shopping{
          background-color: rgba(0,0,0,0.8);
          color: #fff;
         /* text-decoration: none;*/
          padding: 10px;
          border-radius: 3px;
        }
        
        #continue_shopping:hover{
          background-color: rgba(0,0,0,1);
        }
      </style>
    <?php endif; ?>
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    
    <script>
      $(document).ready(function () {
        $("#searchbox").keyup(function () { //when press a keyboard key
          var key = $(this).val(); //#searchbox value is assigned to a variable
          
          $.ajax({ //perform an async AJAX request
            url:'search.php', //send request to search.php
            type:'GET',  //method="get"
            data:'keyword='+key, //send in request keyword=[value of the input field]
            success:function (response) { // if success response returned
              $("#results").html(response); //inject into div "results"
            }//END success:function()
          });//END $.ajax()
        });//END $("#searchbox").keyup(function 
        
        //clear results when input is not focused, except on #results
        // *:not   means everything but not 
        $("*:not(#results)").click(function () {
          $('#results').html('');
          //$('#results').empty(); //same as line above
        });
        
      });//END $(document).ready(function ()
      
    </script>

  </head>