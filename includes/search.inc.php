<!-- search field -->
<div class="search">
  <form action="shop_coffee.php" 
        id="search_form"
        method="get" 
        novalidate="novalidate" 
        autocomplete="off">
        
    <input type="text"
           id="searchbox" 
           name="keyword" 
           maxlength="255" 
           placeholder="Search product by name" />&nbsp; <!-- name="keyword" is used for $_GET['keyword'] -->
           
    <input id="search_button" type="submit" value="search" />
    
    <div id="results"></div><!-- live search results will appear here -->
    
  </form>
  <br />
  <div id="under_search_line"></div>
</div><!--END div.search -->