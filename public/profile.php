<?php

/**
* Capstone Project PHP
* @file profile.php
* @author Alex Ten
* created_at 2018-09-05
**/

require __DIR__ . '/../config.php';
require '../functions.php';

$title = 'Profile';

$active_page = 'profile';

//var_dump($_SESSION); //to check what data is comming to the page

//if no $_SESSION['logged_in'] found, which is created only after successful login page
//this is to prevent displaying this page if a user types address www.server_name/profile.php without login process
if(!isset($_SESSION['logged_in'])) {
  header('Location: login.php'); //login page
  die;//always die after redirection
}


if(!empty($_SESSION['success'])){//check if $_SESSION['success'] has value (except 0, NULL, false, empty string)
  $flash_message_success = $_SESSION['success'];
  unset($_SESSION['success']);//delete $_SESSION['success'], NOT whole session //so if you refresh the page you will not see flash message again, but still logged in
} 
elseif (!empty($_SESSION['profile_edited'])){//check if $_SESSION['success'] has value (except 0, NULL, false, empty string)
  $flash_message_success = 'Your profile is updated!';
  unset($_SESSION['profile_edited']);//delete $_SESSION['success'], NOT whole session //so if you refresh the page you will not see flash message again, but still logged in
}


$user_info = getUser($dbh, $_SESSION['user_id']); //now all info about user is stored in a variable, so can erase all data from $_SESSION, except $_SESSION['logged_in']
//var_dump($user_info); //to check if query works properly and array has all info about specific user
//unset($_SESSION['user_id']); //if unset user id, will not be able display user's personal info
//var_dump($_SESSION); //to check if no data is in $_SESSION left, except $_SESSION['logged_in']
?>

<!-- <head> -->
<?php include '../includes/head.inc.php' ?>


  <body id="index">

    <div id="wrapper">
    
      <!-- header with navigation -->
      <?php include '../includes/header.inc.php' ?>
      
      <main id="content"> <!-- main content goes here -->
        
        <h1><?=$title?></h1>
        
        <?php if(!empty($flash_message_success)) echo "<h2 id=\"flash_message_success\">$flash_message_success</h2>"; ?>
        
        <div id="user_info">
          <h2>User Information:</h2>
          
          <ul>
            <li><strong>First Name:</strong> <?php echo $user_info['first_name']?></li>
            <li><strong>Last Name:</strong> <?php echo$user_info['last_name']?></li>
            <li><strong>Street:</strong> <?php echo $user_info['street']?></li>
            <li><strong>City:</strong> <?php echo $user_info['city']?></li>
            <li><strong>Postal Code:</strong> <?php echo $user_info['postal_code']?></li>
            <li><strong>Province:</strong> <?php echo $user_info['province']?></li>
            <li><strong>Country:</strong> <?php echo $user_info['country']?></li>
            <li><strong>Phone:</strong> <?php echo $user_info['phone']?></li>
            <li><strong>Email:</strong> <?php echo $user_info['email']?></li>
          </ul>
          
          <p id="edit_user_button"><a href="profile_edit.php?user_id=<?php echo $user_info['user_id']; ?>">Edit user info</a></p>
        </div><!--END div#user_info-->
        
        
      </main> <!-- end main#content -->
      
      <!-- footer -->
      <?php include '../includes/footer.inc.php' ?>
    
    </div> <!-- end #wrapper -->
    
  </body>
  
</html>