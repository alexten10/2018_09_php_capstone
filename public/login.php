<?php

/**
* Capstone Project PHP
* @file login.php
* @author Alex Ten
* created_at 2018-09-05
**/

require __DIR__ . '/../config.php';
require '../functions.php';
use \Classes\Utility\Validator;
$vldtr = new Validator();

$title = 'LogIn';

$active_page = 'login';

//var_dump($_SESSION); //to see what data comes first in $_SESSION array


//testing the $_POST (if have POST)
if($_SERVER['REQUEST_METHOD'] == 'POST') {
  /// 1 validate email field for not empty and for correct email format
  $vldtr->validateEmail('email'); //this is 1
  $vldtr->required('email'); //this is 2
  
  /// 2 validate password field for not empty
  $vldtr->required('password');
  
  
  //if no errors found
  if(empty($vldtr->errors())) {
  
    //get all info about user from database using typed email
    /// 1. connecting to mysql db "coffeccino_db" (see config.php)
    /// 2. to show errors within db if errors occur (see config.php)
    /// 3 make query for database (see in functions.php)
    $user_info = getUserByEmail($dbh, $_POST['email']);
    //var_dump($user_info); //to check if query works properly and array has all info about specific user
    
    
    //compare typed password with the password stored in database
    /// 1 if passwords match (remember if there is no user were found before, it will compare typed password against empty value in the array)
    if(password_verify($_POST['password'], $user_info['password'])) {
      $greeting = "Welcome, {$user_info['first_name']}!";
      $_SESSION['success'] = $greeting;//if email and password match, set $_SESSION['success'] to exist
      $_SESSION['logged_in'] = true;
      //$_SESSION['email'] = $_POST['email']; // put typed email into SESSION array to use it in a query on profile.php,
                                              // but it contains sensitive info (users email), so use user id
      $_SESSION['user_id'] = $user_info['user_id'];//non-sensitive info, but it can identify user to get info about user from database by id
      session_regenerate_id();//regenerate session id
      header('Location: profile.php');//redirect to profile.php
      die;//if redirect, then die (stop) this page loading
    }//END if
    /// 2 if passwords dont match OR no info found in database about user by email
    else {
      $_SESSION['no_success'] = 'Sorry, credentials don\'t match!';
      $flash_message_no_success = $_SESSION['no_success'];
      unset($_SESSION['no_success']);
    }//END else
    
  }//END if(empty($vldtr->errors())) 
  
}//END if($_SERVER['REQUEST_METHOD'] == 'POST')


//if $_SESSION['logged_out'] exists, which comes form logout.php
if(isset($_SESSION['logged_out'])){
  $_SESSION['logged_out'] = 'You have logged out.';
  $flash_message_logout = $_SESSION['logged_out'];
  unset ($_SESSION['logged_out']);
}


$errors = $vldtr->errors();
?>

<!-- <head> -->
<?php include '../includes/head.inc.php' ?>


  <body id="index">

    <div id="wrapper">
    
      <!-- header with navigation -->
      <?php include '../includes/header.inc.php' ?>
      
      <main id="content"> <!-- main content goes here -->

        <h1><?=$title?></h1>
        
        <!-- show error flash message when login is not successful -->
        <?php if(!empty($flash_message_no_success)) echo "<h2 id=\"flash_message_no_success\">$flash_message_no_success</h2>"; ?><!--show message for unsuccessful log in-->
        
        <?php if(!empty($flash_message_logout)) echo "<h2 id=\"flash_message_success\">$flash_message_logout</h2>"; ?> <!--show message for success log out, css is for id="falsh_message_success"-->
        
          <form method="post"
                action="login.php"
                id="login"
                name="login"
                accept-charset="utf-8" 
                novalidate="novalidate">
            
            <fieldset>
              <legend><b>LogIn</b></legend>
              
              <p>
                <label for="email">Email Address</label>
                <input type="email"
                       name="email"
                       id="email"
                       value="<?php 
                                if (!empty($_POST['email'])) {
                                  echo esc ($_POST['email']);
                                }
                              ?>"
                       placeholder="Enter your registered email" /><br />
                       
                       <?php if(!empty($errors['email'])) : ?>
                        <span class="error"><?=$errors['email']?></span><br />
                      <?php endif; ?>
              </p>

              <p>
                <label for="pass">Password</label>
                <input type="password"
                       id="pass"
                       name="password"
                       maxlength="255"
                       value=""
                       placeholder="Enter your password" /><br />
                       
                <?php if(!empty($errors['password'])) : ?>
                  <span class="error"><?=$errors['password']?></span><br />
                <?php endif; ?>
              </p>
              
            </fieldset>
          
          
            <p id="form_submit_buttons">
              <input type="submit" value="Login" class="button" />&nbsp; &nbsp;
              <input type="reset" value="Clear Form" class="button" />
            </p>
          
          </form>
        
        
      </main> <!-- end main#content -->
      
      <!-- footer -->
      <?php include '../includes/footer.inc.php' ?>
    
    </div> <!-- end #wrapper -->
    
  </body>
  
</html>