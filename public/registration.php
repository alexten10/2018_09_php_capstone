<?php

/**
* Capstone Project PHP
* @file registration.php
* @author Alex Ten
* created_at 2018-09-05
**/

require __DIR__ . '/../config.php';
require '../functions.php';
use \Classes\Utility\Validator;
$vldtr = new Validator;
//var_dump($vldtr); //checking if $vldtr exists

$title = 'Registration';
$active_page = 'registration';



//testing the $_POST (if have POST)
if($_SERVER['REQUEST_METHOD'] == 'POST') {
  
  // !!! order of calling functions is important !!!
  
  $vldtr->validateForSymbols('first_name');
  $vldtr->required('first_name');
  
  $vldtr->validateForSymbols('last_name');
  $vldtr->required('last_name');
  
  $vldtr->validateStreetSymbols('street');
  $vldtr->required('street');
  
  $vldtr->validateForSymbols('city');
  $vldtr->required('city');
  
  $vldtr->maxLength('postal_code', 12);
  $vldtr->validatePostalCode('postal_code');
  $vldtr->required('postal_code');
  
  $vldtr->validateForSymbols('province');
  $vldtr->required('province');
  
  $vldtr->validateForSymbols('country');
  $vldtr->required('country');
  
  $vldtr->validatePhone('phone');
  $vldtr->required('phone');
  
  $vldtr->validateEmail('email'); //this is 1
  $vldtr->required('email'); //this is 2
  
  // !!! order of calling functions is important !!!
  $vldtr->validatePasswordStrength('password');
  $vldtr->minLength('password', 6);
  $vldtr->maxLength('password', 12);
  $vldtr->required('password');
  $vldtr->passwordsMatch('password', 'password_confirm');
  
  
  //if no errors found
  if(empty($vldtr->errors())) {
    
    
    //START check if email is unique (is not already exists in the db)
    $query = 'SELECT email
              FROM users
             ';
    $stmt = $dbh->prepare($query);
    $stmt->execute();
    $emails = $stmt->fetchAll(PDO::PARAM_STR);
    
    foreach ($emails as $key=>$value) {
      //set variable if found matching email in db
      if(($_POST['email'] == $value['email'])) {
        $match_found = true; 
        break;
      } //else {echo 'nothing';}
    }
    //END check if email is unique (is not already exists in the db)
    
    //if variable (that shows existing duplicate email in db) doesnt exists
    if(!isset($match_found)) {
      //add values from input fields into the table 'users' in 'coffeccino_db' database
      $query = "INSERT INTO
                users
                (
                first_name,
                last_name,
                street,
                city,
                postal_code,
                province,
                country,
                phone,
                email,
                password
                )
                VALUES
                (
                :first_name,
                :last_name,
                :street,
                :city,
                :postal_code,
                :province,
                :country,
                :phone,
                :email,
                :password
                )";
      $stmt = $dbh->prepare($query);// prepare query (prepare SQL statement above to be executed)
      
      // bind (connecting) values of validated input fields with database values
      $stmt->bindValue(':first_name', $_POST['first_name'], PDO:: PARAM_STR);
      $stmt->bindValue(':last_name', $_POST['last_name'], PDO:: PARAM_STR);
      $stmt->bindValue(':street', $_POST['street'], PDO:: PARAM_STR);
      $stmt->bindValue(':city', $_POST['city'], PDO:: PARAM_STR);
      $stmt->bindValue(':postal_code', $_POST['postal_code'], PDO:: PARAM_STR);
      $stmt->bindValue(':province', $_POST['province'], PDO:: PARAM_STR);
      $stmt->bindValue(':country', $_POST['country'], PDO:: PARAM_STR);
      $stmt->bindValue(':phone', $_POST['phone'], PDO:: PARAM_STR);
      $stmt->bindValue(':email', $_POST['email'], PDO:: PARAM_STR);
      $password_encrypted = password_hash($_POST['password'], PASSWORD_DEFAULT);
      $stmt->bindValue(':password', $password_encrypted, PDO:: PARAM_STR);
      
      //checking if INSERT works before proceeding the next step
      //$stmt->execute();
      //die ('yeah, INSERT query works');
      
      // if INSERT works
      if($stmt->execute()) {
        
        //get ID of the last inserted row to assign new IDs for new rows
        $id = $dbh->lastInsertId();
        
        //query for assigning IDs for new records (INSERTs)
        $query = "SELECT *
                  FROM
                  users
                  WHERE user_id = :id";
        
        $stmt = $dbh->prepare($query);//prepare query (prepare SQL statement to be executed)
        $stmt->bindValue(':id', $id, PDO::PARAM_INT);//bind ID value to the query
        $stmt->execute();//execute query
        $user = $stmt->fetch(PDO::FETCH_ASSOC);// fetch (get) result as associative array
        $done = true;//declare sucessful flag (sign) after all above is done
      }// END if INSERT works. "if($stmt->execute())" starting line
      
      else{
        die ("can't insert a new record");
      }//END else
    }//END if(!isset($match_found)) 
    
    //if variable (that shows existing duplicate email in db) exists
    else {
      $email_exist_msg = "Sorry, email has already been registered before";
    }//END else
    
  }//END if no errors. "if(count($errors) == 0)" starting line
    
}//END if test for $_POST. "if($_SERVER['REQUEST_METHOD'] == 'POST')" starting line

$errors = $vldtr->errors();
?>

<!-- <head> -->
<?php include '../includes/head.inc.php' ?>


  <body id="index">
   
    <div id="wrapper">
    
      <!-- header with navigation -->
      <?php include '../includes/header.inc.php' ?>
      
      <main id="content"> <!-- main content goes here -->
        
        <?php if (empty($done)) : ?>
        
          <h1><?=$title?></h1>
          
          <form method="post"
                action="registration.php"
                id="registration_info"
                name="registration_info"
                accept-charset="utf-8" 
                novalidate="novalidate">
            
            <fieldset>
              <legend><b>Registration Info</b></legend>
              
              <p>
                <label for="first_name">First Name</label><!-- name of the form -->
                <!-- input field is "sticky", which means if validation error appears,
                the typed in the field data will not be earased-->
                <input type="text"
                       id="first_name"
                       name="first_name"
                       maxlength="255"
                       value="<?php 
                                if(!empty($_POST['first_name'])){
                                  echo esc ($_POST['first_name']);}
                              ?>"
                       placeholder="Type your first name" /><br />
                
                <!-- display error message near the field -->
                <?php if(!empty($errors['first_name'])) : ?>
                  <span class="error"><?=$errors['first_name']?></span><br />
                <?php endif; ?>
              </p>

              <p>
                <label for="last_name">Last Name</label>
                <input type="text"
                       id="last_name"
                       name="last_name"
                       maxlength="255"
                       value="<?php 
                                if(!empty($_POST['last_name'])){
                                  echo esc ($_POST['last_name']);}
                              ?>"
                       placeholder="Type your last name" /><br />
                       
                <?php if(!empty($errors['last_name'])) : ?>
                  <span class="error"><?=$errors['last_name']?></span><br />
                <?php endif; ?>
              </p>

              <p>
                <label for="street">Street</label>
                <input type="text"
                       id="street"
                       name="street"
                       maxlength="255"
                       value="<?php 
                                if(!empty($_POST['street'])){
                                  echo esc ($_POST['street']);}
                              ?>"
                       placeholder="Type your street" /><br />
                       
                <?php if(!empty($errors['street'])) : ?>
                  <span class="error"><?=$errors['street']?></span><br />
                <?php endif; ?>
              </p>

              <p>
                <label for="city">City</label>
                <input type="text"
                       id="city"
                       name="city"
                       maxlength="255"
                       value="<?php 
                                if(!empty($_POST['city'])){
                                  echo esc ($_POST['city']);}
                              ?>"
                       placeholder="Type your city" /><br />
                       
                <?php if(!empty($errors['city'])) : ?>
                  <span class="error"><?=$errors['city']?></span><br />
                <?php endif; ?>
              </p>

              <p>
                <label for="postal_code">Postal Code</label>
                <input type="text"
                       id="postal_code"
                       name="postal_code"
                       maxlength="255"
                       value="<?php 
                                if(!empty($_POST['postal_code'])){
                                  echo esc ($_POST['postal_code']);}
                              ?>"
                       placeholder="Type your postal code" /><br />
                       
                <?php if(!empty($errors['postal_code'])) : ?>
                  <span class="error"><?=$errors['postal_code']?></span><br />
                <?php endif; ?>
              </p>

              <p>
                <label for="province">Province</label>
                <input type="text"
                       id="province"
                       name="province"
                       maxlength="255"
                       value="<?php 
                                if(!empty($_POST['province'])){
                                  echo esc ($_POST['province']);}
                              ?>"
                       placeholder="Type your province" /><br />
                       
                <?php if(!empty($errors['province'])) : ?>
                  <span class="error"><?=$errors['province']?></span><br />
                <?php endif; ?>
              </p>

              <p>
                <label for="country">Country</label>
                <input type="text"
                       id="country"
                       name="country"
                       maxlength="255"
                       value="<?php 
                                if(!empty($_POST['country'])){
                                  echo esc ($_POST['country']);}
                              ?>"
                       placeholder="Type your country" /><br />
                       
                <?php if(!empty($errors['country'])) : ?>
                  <span class="error"><?=$errors['country']?></span><br />
                <?php endif; ?>
              </p>

              <p>
                <label for="phone">Phone</label>
                <input type="tel"
                       id="phone"
                       name="phone"
                       maxlength="30"
                       value="<?php 
                                if(!empty($_POST['phone'])){
                                  echo esc ($_POST['phone']);}
                              ?>"
                       placeholder="(000)222-3344"/><br />
                       
                <?php if(!empty($errors['phone'])) : ?>
                  <span class="error"><?=$errors['phone']?></span><br />
                <?php endif; ?>
              </p>

              <p>
                <label for="email">Email Address</label>
                <input type="email"
                       name="email"
                       id="email"
                       value="<?php 
                                if(!empty($_POST['email'])){
                                  echo esc ($_POST['email']);}
                              ?>"
                       placeholder="your_email@example.com" /><br />
                       
                <?php if(!empty($errors['email'])) : ?>
                  <span class="error"><?=$errors['email']?></span><br />
                <?php endif; ?>
                
                <!-- if email already is in db, show message -->
                <?php if(isset($email_exist_msg)) : ?>
                  <span class="error"><?php echo $email_exist_msg ?></span><br />
                <?php endif; ?>
              </p>

              <p>
                <label for="pass">Password</label>
                <input type="password"
                       id="pass"
                       name="password"
                       maxlength="255"
                       value="<?php 
                                if(!empty($_POST['password'])){
                                  echo esc ($_POST['password']);}
                              ?>"
                       placeholder="Set your password (min 4 characters)" /><br />
                       
                <?php if(!empty($errors['password'])) : ?>
                  <span class="error"><?=$errors['password']?></span><br />
                <?php endif; ?>
              </p>

              <p>
                <label for="pass_confirm">Confirm Password</label>
                <input type="password"
                       id="pass_confirm"
                       name="password_confirm"
                       maxlength="255"
                       value="<?php 
                                if(!empty($_POST['password_confirm'])){
                                  echo esc ($_POST['password_confirm']);}
                              ?>"
                       placeholder="Type again your password" />
              </p>

            </fieldset>
          
            <p id="form_submit_buttons">
              <input type="submit"
                     value="Send Form"
                     class="button" />&nbsp; &nbsp;
              <input type="reset"
                     value="Clear Form"
                     class="button" />
            </p>
          
          </form>
        
        <?php else : ?>
          <h1>Thank you for registration</h1>
          
          <p>You submitted the following information:</p>
          
          <!-- output only specific results in array manually -->
          <ul>
            <li><strong>First Name:</strong> <?=$user['first_name']?></li>
            <li><strong>Last Name:</strong> <?=$user['last_name']?></li>
            <li><strong>Street:</strong> <?=$user['street']?></li>
            <li><strong>City:</strong> <?=$user['city']?></li>
            <li><strong>Postal Code:</strong> <?=$user['postal_code']?></li>
            <li><strong>Province:</strong> <?=$user['province']?></li>
            <li><strong>Country:</strong> <?=$user['country']?></li>
            <li><strong>Phone:</strong> <?=$user['phone']?></li>
            <li><strong>Email:</strong> <?=$user['email']?></li>
          </ul>
          
          <p>Now you can login using your email and password.</p>
        <?php endif; ?>
        
      </main> <!-- end main#content -->
      
      <!-- footer -->
      <?php include '../includes/footer.inc.php' ?>
    
    </div> <!-- end #wrapper -->
    
  </body>
  
</html>


