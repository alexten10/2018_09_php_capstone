<?php

/**
* Capstone Project PHP
* @file index.php
* @author Alex Ten
* created_at 2018-09-05
**/

require __DIR__ . '/../config.php';
//echo APP; // to check __DIR__ .



$title = 'Home';

$active_page = 'index';

?>

<!-- <head> -->
<?php include '../includes/head.inc.php' ?>


  <body id="index">
   
    <div id="wrapper">

      <!-- header with navigation -->
      <?php include '../includes/header.inc.php' ?> 
      
      <main id="content"> <!-- main content goes here -->
        
        <?php include '../includes/search.inc.php' ?>
        
        <?php if(isset($_SESSION['cart'])) { //if anything is added in cart
          include '../includes/cart.inc.php';
        } ?>
        
        <h1><?=$title?></h1>

        <p class="row"><img src="images/main_breakfast.jpg" alt="breakfast" /></p>
        <div class="text"><p class="row">COFFEE FOR BREAKFAST</p></div>
        <div class="all_row"></div>
      
        <p class="row">COFFEE FOR LEISURE</p>
        <p class="row"><img src="images/main_beach.jpg" alt="beach" /></p>
        <div class="all_row"></div>
              
        <p class="row"><img src="images/main_family.jpg" alt="family" /></p>
        <p class="row">COFFEE FOR YOU AND YOUR FAMILY</p>
        <div class="all_row"></div>
        
        <p id="bottom_paragraph">COFFEE FOR EVERYTHING</p>
      </main> <!-- end main#content -->
      
      <!-- footer -->
      <?php include '../includes/footer.inc.php' ?>
    
    </div> <!-- end #wrapper -->
    
  </body>
  
</html>