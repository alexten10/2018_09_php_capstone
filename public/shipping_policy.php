<?php

/**
* Capstone Project PHP
* @file shipping_policy.php
* @author Alex Ten
* created_at 2018-09-05
**/

require __DIR__ . '/../config.php';

$title = 'Shipping Policy';

$active_page = 'shipping_policy';

?>

<!-- <head> -->
<?php include '../includes/head.inc.php' ?>


  <body id="index">

    <div id="wrapper">
    
      <!-- header with navigation -->
      <?php include '../includes/header.inc.php' ?>
      
      <main id="content"> <!-- main content goes here -->
        
        <?php include '../includes/search.inc.php' ?>
        
        <?php if(isset($_SESSION['cart'])) { //if anything is added in cart
          include '../includes/cart.inc.php';
        } ?>
        
        <h1><?=$title?></h1>
        <br/>
        
        <h2>Order Tracking</h2>
        <p>
          All shipments will receive a tracking number once shipped, this tracking number will be 
          emailed to the email address you provide during registration.
        </p>
        <br />
        
        <h2>Shipping Times</h2>
        <p>
          Shipping times displayed on the website are best-guess approximations. Due to courier 
          schedules, holiday hours of operations and fluctuations in shipping volumes, 
          shipping estimates may be off. If you MUST have a product by a specific date, 
          please include a comment during checkout and we will try to work with you to 
          accommodate the deadline, or let you know if we are unable to.
        </p>
        <br />
        
        <h2>Order Processing Times</h2>
        <p>
          We typically aim for a 24 hours processing time, but a number of factors may 
          affect that and we don't guarantee it. Order processing can be affect by order 
          volumes at different times of year, surges in order volume during holiday seasons 
          or promotions, and inventory status. Mondays are especially busy days (Tuesday in the 
          case of long-weekends) and may add a day to the processing time. Once you receive an 
          email that your order has been placed in "Processing", no further changes can be 
          made. Replying to emails we send may add a delay to processing as your order is 
          automatically placed on hold so that we can reply to your email.
        </p>
        <br />
        
      </main> <!-- end main#content -->
      
      <!-- footer -->
      <?php include '../includes/footer.inc.php' ?>
    
    </div> <!-- end #wrapper -->
    
  </body>
  
</html>