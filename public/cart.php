<?php

/**
* Capstone Project PHP
* @file cart.php
* @author Alex Ten
* created_at 2018-09-05
**/

//var_dump($_POST); //to check if the page gets product_id which is sent by pressing 'add to cart'

require __DIR__ . '/../config.php'; //main config file
require '../functions.php';  //main functions file

$title = 'Cart';

$active_page = 'cart';


//var_dump($_POST);


//if have $_GET['clear'] after pressing 'clear cart' button on this page
if(isset($_GET['clear'])) {
  unset ($_SESSION['cart']);
  session_regenerate_id();
}

//var_dump($_POST['product_id']); check ig get product_id after pressing button "Add to cart"
if(!empty($_POST['product_id'])) {
  $product = getProduct($dbh, $_POST['product_id']);
  $quantity = $_POST['quantity'];
  
  //if quantity in db less then user wants to purchase
  if($quantity > $product['quantity_available']) {
    $_SESSION['no_quantity'] = true;
    header('Location: detail.php?product_id=' . $_POST["product_id"]);
    die;
  }
  
  $cart = [
           'product_id' => $_POST['product_id'],
           'title' => $product['title'],
           'price' => $product['price'],
           'image' => $product['image'],
           'quantity' => $quantity
          ];
  //var_dump($cart);
  $_SESSION['cart'] = $cart;//assign $cart to the session, $_POST exists only during first opening of this page
}

//var_dump($_SESSION);

?>

<!-- <head> -->
<?php include '../includes/head.inc.php' ?>


  <body id="index">

    <div id="wrapper">
    
      <!-- header with navigation -->
      <?php include '../includes/header.inc.php' ?>
      
      <main id="content"> <!-- main content goes here -->
        
        <?php include '../includes/search.inc.php' ?>
        
        <h1><?=$title?></h1>

        <div id="cart_content">
          
          <h3 class="cart_h3">YOUR CART</h3>
          
          
          <!--###############################################################-->
          <!--************************ START if ****************************-->
          <?php if(!isset($_SESSION['cart'])) : ?><!--if no $_SESSION['cart'] - display this-->
            <p class="cart_paragraph">Your cart is empty</p>
            <p class="continue_shopping">
              <a href="shop_coffee.php"
                 title="shop_coffee"
                 id="continue_shopping" 
                 style="text-decoration: none;"> <!-- inline style -->
                   CONTINUE SHOPPING COFFEE
              </a>
            </p>
            
          <?php else : ?>
            <div class="cart">
              <p><i>You have <strong>1</strong> item in your cart.</i></p>
              <p>
                <img src="images/coffee_images/<?php echo $_SESSION['cart']['image'] ?>"
                     alt="<?php echo $_SESSION['cart']['title'] ?>" /><br />
              </p>
              <p><strong>Item name:</strong> <?php echo $_SESSION['cart']['title'] ?></p>
              <p><strong>Item price:</strong> <?php echo $_SESSION['cart']['price'] ?>$</p>
              <p><strong>Quantity:</strong> <?php echo $_SESSION['cart']['quantity'] ?></p>
              <p class="checkout"><a href="checkout.php">Checkout</a></p>
              <p class="checkout"><a href="cart.php?clear=1">Clear cart</a></p><!--send to this page $_GET['clear'] = 1-->
            </div><!-- END div.cart-->
            
          <?php endif; ?>
          <!--********************** END if ****************************-->
          <!--############################################################-->
          
          
        </div><!-- END div#cart_content-->
        
      </main> <!-- end main#content -->
      
      <!-- footer -->
      <?php include '../includes/footer.inc.php' ?>
    
    </div> <!-- end #wrapper -->
    
  </body>
  
</html>