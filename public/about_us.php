<?php

/**
* Capstone Project PHP
* @file about_us.php
* @author Alex Ten
* created_at 2018-09-05
**/

require __DIR__ . '/../config.php';

$title = 'About Us';

$active_page = 'about_us';

?>

<!-- <head> -->
<?php include '../includes/head.inc.php' ?>


  <body id="index">

    <div id="wrapper">
    
      <!-- header with navigation -->
      <?php include '../includes/header.inc.php' ?>
      
      <main id="content"> <!-- main content goes here -->
        
        <?php include '../includes/search.inc.php' ?>
        
        <?php if(isset($_SESSION['cart'])) { //if anything is added in cart
          include '../includes/cart.inc.php';
        } ?>
        
        <h1><?=$title?></h1>
        <p>
          Coffeeccino is Canada's local coffee retailer.
          Wherever you happen to live in this great country, we want to supply you with
          the world's best coffee. Organic, Fair Trade and Freshly Roasted, we deliver 
          amazing coffee right to your mailbox.
          Delicious, convenient and most importantly, affordable! 
        </p>
      </main> <!-- end main#content -->
      
      <!-- footer -->
      <?php include '../includes/footer.inc.php' ?>
    
    </div> <!-- end #wrapper -->
    
  </body>
  
</html>