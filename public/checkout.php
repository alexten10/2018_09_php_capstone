<?php

/**
* Capstone Project PHP
* @file check_out.php
* @author Alex Ten
* created_at 2018-09-09
**/

require __DIR__ . '/../config.php';

$title = 'Checkout';

$active_page = 'checkout';



//var_dump($_SESSION);

//if anything is placed in the cart, we have $_SESSION['cart']
//this prevents of displaying this page (checkout.php) if a user types address of this page manually
if(!isset($_SESSION['cart'])) {
  header ('Location:shop_coffee.php');
  die;
}


if(isset($_SESSION['logged_in'])) {
  define('PST', .08);
  define('GST', .05);

  $subtotal = $_SESSION['cart']['quantity'] * $_SESSION['cart']['price'];
  //print_r ($subtotal);
  $pst = $_SESSION['cart']['quantity'] * $_SESSION['cart']['price'] * PST;
  $pst = round($pst,2);//round decimal number to 2 numbers after .(point), which is 100th (cents)
  //echo $pst;
  $gst = $_SESSION['cart']['quantity'] * $_SESSION['cart']['price'] * GST;
  $gst = round($gst,2);
  //echo $gst;
  $total = $subtotal + $pst + $gst;
  //echo $total;
  
  $_SESSION['pst'] = $pst;
  $_SESSION['gst'] = $gst;
  $_SESSION['subtotal'] = $subtotal;
  $_SESSION['total'] = $total;

  //var_dump($_SESSION);
}//END if(isset($_SESSION['logged_in']))


?>

<!-- <head> -->
<?php include '../includes/head.inc.php' ?>


  <body id="index">

    <div id="wrapper">
    
      <!-- header with navigation -->
      <?php include '../includes/header.inc.php' ?>
      
      <main id="content"> <!-- main content goes here -->
        
        <?php include '../includes/search.inc.php' ?>
        
        <h1><?=$title?></h1>
        
        
        <!--****************** START if **************************-->
        <?php if(!isset($_SESSION['logged_in'])) : ?>
          <h2>You must be registered and logged in before checkout!</h2>
          
        <?php else : ?>
          <p>Subtotal: <span class="sum">$<?php echo $subtotal ?></span></p>
          <p>GST: <span class="sum">$<?php echo $gst ?></span></p>
          <p>PST: <span class="sum">$<?php echo $pst ?></span></p>
          <hr><!--horizontal ruler-->
          <h2>Total: <span class="sum">$<?php echo $total ?></span></h2>
          <p id="pay_button"><a href="payment.php">Pay my order</a></p>
          
        <?php endif; ?>
        <!--****************** END if **************************-->
        
        
      </main> <!-- end main#content -->
      
      <!-- footer -->
      <?php include '../includes/footer.inc.php' ?>
    
    </div> <!-- end #wrapper -->
    
  </body>
  
</html>