<?php

ini_set('display_errors', 1);
ini_set('error_reporting', E_ALL);

session_start();
ob_start();

//this session might come from user(regular) site
if(isset($_SESSION['logged_in'])) {
  unset ($_SESSION['logged_in']);
  session_regenerate_id();
}

//this session might come from user (regular) site
if(isset($_SESSION['cart'])) {
  unset ($_SESSION['cart']);
  session_regenerate_id();
}

//this session might come from user (regular) site
if(isset($_SESSION['total'])) {
  unset ($_SESSION['total']);
  session_regenerate_id();
}



//define constants
define ('APP', __DIR__);
define('DB_USER', 'root');
define('DB_PASS', '');
#define('DB_USER', 'admin'); // this is for if you want to have access by username and password
#define('DB_PASS', 'mypass');
define('DB_DSN', 'mysql:host=localhost;dbname=coffeeccino_db');

// 1. connecting to mysql db "coffeccino_db"
$dbh= new PDO(DB_DSN, DB_USER, DB_PASS);
// 2. to show errors within db if errors occur
$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);


//https://www.php-fig.org/psr/psr-0/
function autoload($className)
{
    $className = ltrim($className, '\\');
    $fileName  = '';
    $namespace = '';
    if ($lastNsPos = strrpos($className, '\\')) {
        $namespace = substr($className, 0, $lastNsPos);
        $className = substr($className, $lastNsPos + 1);
        $fileName  = str_replace('\\', DIRECTORY_SEPARATOR, $namespace) . DIRECTORY_SEPARATOR;
    }
    $fileName .= str_replace('_', DIRECTORY_SEPARATOR, $className) . '.php';

    require $fileName;
}
spl_autoload_register('autoload');


