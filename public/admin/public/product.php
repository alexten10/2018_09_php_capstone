<?php

/**
* ------ BACKEND! ------
* 
* Capstone Project PHP
* @file admin/public/product.php
* @author Alex Ten
* created_at 2018-09-11
**/

require __DIR__ . '/../config_admin.php'; //main config file
require '../functions_admin.php';  //main functions file

$title = 'Product';
$active_page = 'product';

//var_dump($_SESSION);

if(!isset($_SESSION['logged_admin'])){
  header ('Location: login.php');
  die;
}


if(isset($_SESSION['success'])){
  $flash_msg = $_SESSION['success'];
  unset ($_SESSION['success']);
  session_regenerate_id();
}


if(isset($_SESSION['product_created'])) {
  $flash_msg = "A new product is created successfully!";
  unset ($_SESSION['product_created']);
  session_regenerate_id();
}


if(isset($_SESSION['product_edited'])) {
  $flash_msg = "The product is edited successfully!";
  unset ($_SESSION['product_edited']);
  session_regenerate_id();
}


if(!empty($_GET['product_id'])) {
  $product = getProduct ($dbh, $_GET['product_id']);
}

// if get keyword from search field
elseif(!empty($_GET['keyword'])) {
  $keyword = strip_tags(htmlspecialchars($_GET['keyword'])); //assign $_GET['keyword'] value to a variable, after escaping special chars and removing tags for security reasons
  $keyword="%$keyword%"; //search including any chars before typed keyword (%$keyword) any chars after typed ($keyword%)
  $products = search($dbh, $keyword); //send to the search function: database and value of $keyword
  $search_value = strip_tags(htmlspecialchars($_GET['keyword']));
  $search_result_count = count($products);//number of items(subarrays)
  //var_dump($products);
}

else {
  $products = getAllProducts ($dbh);
}



?><!doctype html>

<html lang="en">

  <head>
    
    <title><?php echo $title; ?></title>
    <meta charset="utf-8" />
    <meta name="viewport"
          content="width=device-width, initial-scale=1" />
          
    <link rel="shortcut icon" href="../../images/favicon64.png" type="image/png" /><!-- favorite icon in title link -->
    
    <!-- link to css file for desktops -->
    <link rel="stylesheet"
          type="text/css"
          href="styles/desktop_admin.css"
          media="screen and (min-width: 768px)"
    />
    
    <!-- link to css file for mobiles -->
    <link rel="stylesheet"
          type="text/css"
          href="styles/mobile_admin.css"
          media="screen and (max-width: 767px)"
    /> 
    
    <!-- CSS link for IE browser version 9 and less -->
    <!--[if LTE IE 9]>
          <link rel="stylesheet"
          type="text/css"
          href="styles/desktop_admin.css"
          media="screen"
          />
    <![endif] --> 
    
    <!-- link to css file for printers -->
    <link rel="stylesheet"
          type="text/css"
          href="styles/print.css" 
          media="print" 
    />
    
    
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    
    <script>
      //function for search field
      $(document).ready(function () {
        $("#searchbox").keyup(function () { //when press a keyboard key
          var key = $(this).val(); //#searchbox value is assigned to a variable
          
          $.ajax({ //perform an async AJAX request
            url:'search.php', //send request to search.php
            type:'GET',  //method="get"
            data:'keyword='+key, //send in request keyword=[value of the input field]
            success:function (response) { // if success response returned
              $("#results").html(response); //inject into div "results"
            }//END success:function()
          });//END $.ajax()
        });//END $("#searchbox").keyup(function 
        
        
        //clear results when input is not focused, except on #results
        // *:not   means everything but not 
        $("*:not(#results)").click(function () {
          $('#results').html('');
          //$('#results').empty(); //same as line above
        });
      });//END $(document).ready(function ()
    </script>
    
  </head>








  <body id="index">
   
    <div id="wrapper">
      
      
      
      <!-- ********************* START header + navigation ************************-->
      <div id="header_nav"> <!-- ***** #header_nav start *****  -->
        <header>
          <div id="logo"><a href="index.php" title="Home admin"><img src="../../images/logo.png" alt="coffeeccino" /></a></div>
          <div id="tagline"><a href="index.php" title="Home admin">Delight in every drop</a></div>
          <div id="user_section"><!-- LOGIN / LOGOUT nav menu-->
            <?php if(!isset($_SESSION['logged_admin'])) {
                echo '<span class="user_menu"><a href="login.php">LogIn</a></span>';
              } else {
                echo '<span class="user_menu"><a href="logout.php">LogOut</a></span>';
              }
            ?>
          </div><!-- END #user_section-->
        </header>

        <nav>
          <div id="menu">
            <a href="#" id="menulink" title="Menu"><!-- hamburger menu -->
              <span id="hamburger_top"></span>
              <span id="hamburger_middle"></span>
              <span id="hamburger_bottom"></span>
            </a>
            <ul id="navlist">
              <li><a href="index.php"
                     <?php if($active_page == 'index') {echo 'class="current"';}?>
                     title="Home Admin">Home Adm</a></li>
              <li><a href="product.php"
                     <?php if($active_page == 'product') {echo 'class="current"';}?>
                     title="Product" >Product</a></li>
              <li><a href="invoice.php"
                     <?php if($active_page == 'invoice') {echo 'class="current"';}?>
                     title="Invoice">Invoice</a></li>
              <li><a href="users.php"
                     <?php if($active_page == 'users') {echo 'class="current"';}?>
                     title="Users">Users</a></li>
              <li><a href="#"
                     <?php if($active_page == '#') {echo 'class="current"';}?>
                     title="Reserved Link">-</a></li>
            </ul>
          </div> <!-- end #menu -->
        </nav>
      </div><!-- ***** end header_nav ***** -->
      <!-- ********************* END header + navigation ************************-->




      <!-- ######################################################################-->
      <!-- /////////////////// START main content ///////////////////////////////-->
      <!-- ######################################################################-->
      <main id="content"> <!-- main content goes here -->
        
        
        
        <!--****************************START search**********************-->
        <div class="search">
          <form action="product.php" 
                id="search_form"
                method="get" 
                novalidate="novalidate" 
                autocomplete="off">
            <input type="text"
                   id="searchbox" 
                   name="keyword" 
                   maxlength="255" 
                   placeholder="Search product by name" />&nbsp; <!-- name="keyword" is used for $_GET['keyword'] -->
            <input id="search_button" type="submit" value="search" />
            <div id="results"></div><!-- live search results will appear here -->
          </form>
          <br />
          <div id="under_search_line"></div>
        </div><!--END div.search --> 
        <!--****************************END search**********************-->
        
        
        <h1 id="admin_msg">THIS IS THE ADMIN SITE !!!</h1>
        <h1 id="title_h1"><?=$title?></h1>
        
        
        
        <!--/////////////// START categories menu ////////////////////-->
        <div class="categories">
          <h2>Options:</h2>
          <ul>
            <li><a href="product_create.php"> - Create new product</a></li>
          </ul>
        </div><!-- div.categories -->
        <hr />
        <!--/////////////// END categories menu //////////////////// -->
        
        
        <?php if(isset($flash_msg)) :?>
          <h2 id="flash_msg"><?php echo $flash_msg ?></h2>
        <?php endif; ?>
        
        
        <!--##########    START if(!isset($_GET['product_id']))               -->
        <?php if (!isset($_GET['product_id'])) : ?><!--if not have $_GET['product_id'] which is for detail view-->
          
          <!-- START if(!empty($_GET['keyword'])) ////////////////-->
          <?php if(!empty($_GET['keyword'])) : ?>
            <h2>You have <?php echo $search_result_count ?> result(s) (deleted = 0) for 
                searching: <?php echo $search_value ?>
            </h2>
          <?php else : ?>
            <h2>All products (deleted = 0)</h2>
          <?php endif; ?>
          <!-- END if(!empty($_GET['keyword'])) /////////////////-->
          
          
          <!--*********************** START all product table *********************-->
          <table id="all_products">
            
            <tr>
              <th>product_id</th>
              <th>title</th>
              <th>image</th>
              <th>price</th>
              <th>category</th>
              <th>quantity_available</th>
              <th>deleted</th>
              <th>options:</th>
            </tr>
            
            <?php foreach ($products as $key) : ?><!-- loop through $products array-->
              <tr>
                <td><?php echo $key['product_id'] ?></td>
                <td><?php echo $key['title'] ?></td>
                <td>
                  <img src="../../images/coffee_images/<?php echo $key['image'] ?>"
                         alt="<?php echo $key['image'] ?>"
                         title="<?php echo $key['image'] ?>"
                         width="40" />
                </td>
                <td><?php echo $key['price'] ?></td>
                <td><?php echo $key['category'] ?></td>
                <td><?php echo $key['quantity_available'] ?></td>
                <td><?php echo $key['deleted'] ?></td>
                <td>
                  <a href="product_edit.php?product_id=<?php echo $key['product_id'] ?>">-edit...</a><br />
                  <a href="product_delete.php?product_id=<?php echo $key['product_id'] ?>">-delete...</a><br />
                  <a href="product.php?product_id=<?php echo $key['product_id'] ?>">-full details...</a>
                </td>
              </tr>
              
            <?php endforeach; ?>
            
          </table>
          <!--*********************** END all product table ***********************-->
        
        <!--#############          ELSE            -->
        <?php else : ?>
          
          <h2>Product details</h2>
          
          <div class="product_detail">
            <img src="../../images/coffee_images/<?php echo $product['image']; ?>" 
                 alt="<?php echo $product['title']; ?>" />
            <p class="title"><strong><?php echo $product['title']; ?></strong></p><br />
          </div><!-- END div.product_detail -->
          
          <div id="ul_details">
            <ul>
              <li><strong>product_id:</strong> <?php echo $product['product_id']; ?></li>
              <li><strong>title:</strong> <?php echo $product['title']; ?></li>
              <li><strong>image:</strong> <?php echo $product['image']; ?></li>
              <li><strong>price:</strong> <?php echo $product['price']; ?></li>
              <li><strong>description:</strong> <?php echo $product['description']; ?></li>
              <li><strong>category:</strong> <?php echo $product['category']; ?></li>
              <li><strong>in_stock:</strong> <?php echo $product['in_stock']; ?></li>
              <li><strong>quantity_available:</strong> <?php echo $product['quantity_available']; ?></li>
              <li><strong>supplier:</strong> <?php echo $product['supplier']; ?></li>
              <li><strong>grade:</strong> <?php echo $product['grade']; ?></li>
              <li><strong>rating:</strong> <?php echo $product['rating']; ?></li>
              <li><strong>deleted:</strong> <?php echo $product['deleted']; ?></li>
              <li><strong>created_at:</strong> <?php echo $product['created_at']; ?></li>
              <li><strong>updated_at:</strong> <?php echo $product['updated_at']; ?></li>
            </ul>
          </div><!-- END div#ul_details -->
          
          <div id="product_detail_options">
            <h2>Options:</h2>
            <p><a href="product_edit.php?product_id=<?php echo $product['product_id'] ?>">-Edit this item</a></p>
            <p><a href="product_delete.php?product_id=<?php echo $product['product_id'] ?>">-Delete this item</a></p>
            <p><a href="product.php">-View all products table</a></p>
          </div><!-- END div #product_detail_options -->
          
        <?php endif; ?>
        <!--##########    END if(!isset($_GET['product_id']))               -->
        
        
      </main>
      <!-- ######################################################################-->
      <!-- /////////////////// END main content ///////////////////////////////-->
      <!-- ######################################################################-->
      
      
      
      <footer>
        <h2>***FOOTER is here***</h2>
      </footer>
      
    </div> <!-- end #wrapper -->
    
  </body>
  
</html>