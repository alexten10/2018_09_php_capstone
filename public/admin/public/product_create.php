<?php

/**
* ------ BACKEND! ------
* 
* Capstone Project PHP
* @file admin/public/product_create.php
* @author Alex Ten
* created_at 2018-09-11
**/

require __DIR__ . '/../config_admin.php'; //main config file
require '../functions_admin.php';  //main functions file
use \Classes\Utility\ValidatorAdmin;
$vldtr = new ValidatorAdmin;
//var_dump($vldtr); //checking if $vldtr exists


$title = 'Product Create';
$active_page = 'product_create';

if(!isset($_SESSION['logged_admin'])){
  header ('Location: login.php');
  die;
}

//var_dump($_POST);

//if this page gets $_POST, which sent by pressing button 'save' on this page
if($_SERVER['REQUEST_METHOD'] == 'POST') {
  
  $vldtr->validateForGeneralRules('title');
  $vldtr->required('title');
  
  $vldtr->validateImage('image');
  $vldtr->required('image');
  
  //function 'required' doesnt accept 0 (empty() sees 0 as false)
  //so use 'requiredNumeric' function  
  $vldtr->validatePrice('price');
  $vldtr->requiredNumeric('price');
  
  $vldtr->validateForGeneralRules('description');
  $vldtr->required('description');
  
  $vldtr->validateCategory('category');
  $vldtr->required('category');
  
  //function 'required' doesnt accept 0 (empty() sees 0 as false)
  //so use 'requiredNumeric' function
  $vldtr->validateBoolean('in_stock');
  $vldtr->requiredNumeric('in_stock');
  
  
  //function 'required' doesnt accept 0 (empty() sees 0 as false)
  //so use 'requiredNumeric' function
  $vldtr->validateForDigits('quantity_available');
  $vldtr->requiredNumeric('quantity_available'); 
  
  $vldtr->validateForGeneralRules('supplier');
  $vldtr->required('supplier');
  
  $vldtr->validateGrade('grade');
  $vldtr->required('grade');
  
  $vldtr->validateRating('rating');
  $vldtr->required('rating');
  
  //function 'required' doesnt accept 0 (empty() sees 0 as false)
  //so use 'requiredNumeric' function
  $vldtr->validateBoolean('deleted');
  $vldtr->requiredNumeric('deleted');
  
  //var_dump($vldtr->errors());
  
  
  //after successful validation(no errors found), do INSERT
  if(empty($vldtr->errors())) {
    $query = "INSERT INTO
              product_coffee
              (
               title,
               image,
               price,
               description,
               category,
               in_stock,
               quantity_available,
               supplier,
               grade,
               rating,
               deleted
              )
              VALUES
              (
               :title,
               :image,
               :price,
               :description,
               :category,
               :in_stock,
               :quantity_available,
               :supplier,
               :grade,
               :rating,
               :deleted
              )
             ";
    $stmt = $dbh->prepare($query);
    
    $stmt->bindValue(':title', $_POST['title'], PDO:: PARAM_STR);
    $stmt->bindValue(':image', $_POST['image'], PDO:: PARAM_STR);
    $stmt->bindValue(':price', $_POST['price'], PDO:: PARAM_STR);
    $stmt->bindValue(':description', $_POST['description'], PDO:: PARAM_STR);
    $stmt->bindValue(':category', $_POST['category'], PDO:: PARAM_STR);
    $stmt->bindValue(':in_stock', $_POST['in_stock'], PDO:: PARAM_STR);
    $stmt->bindValue(':quantity_available', $_POST['quantity_available'], PDO:: PARAM_INT);
    $stmt->bindValue(':supplier', $_POST['supplier'], PDO:: PARAM_STR);
    $stmt->bindValue(':grade', $_POST['grade'], PDO:: PARAM_STR);
    $stmt->bindValue(':rating', $_POST['rating'], PDO:: PARAM_INT);
    $stmt->bindValue(':deleted', $_POST['deleted'], PDO:: PARAM_BOOL);
    
    //if the query correct, check out which product_id number assign for new item
    if($stmt->execute()) {
      $prod_id - $dbh->lastInsertId();//Returns the ID of the last inserted row or sequence value 
      $query = "SELECT *
                FROM product_coffee
                WHERE product_id = :product_id
               ";
      $stmt = $dbh -> prepare($query);
      $stmt -> bindValue(':product_id', $prod_id, PDO::PARAM_INT);
      $stmt -> execute();
      $_SESSION['product_created'] = true; //SESSION as flag of successful creation of new item
      header ('Location: product.php');//redirect to product.php
      die; //stop executing code at this point
    }//END if($stmt->execute())
    
    //if the query incorrect, set var as a flag
    else {
      $product_not_created = 'Can not create new product';
    }// END else
    
  }//END if(empty($vldtr->errors()))
  
}//END if($_SERVER['REQUEST_METHOD'] == 'POST')

$errors = $vldtr->errors();






?><!doctype html>

<html lang="en">

  <head>
    
    <title><?php echo $title; ?></title>
    <meta charset="utf-8" />
    <meta name="viewport"
          content="width=device-width, initial-scale=1" />
          
    <link rel="shortcut icon" href="../../images/favicon64.png" type="image/png" /><!-- favorite icon in title link -->
    
    <!-- link to css file for desktops -->
    <link rel="stylesheet"
          type="text/css"
          href="styles/desktop_admin.css"
          media="screen and (min-width: 768px)"
    />
    
    <!-- link to css file for mobiles -->
    <link rel="stylesheet"
          type="text/css"
          href="styles/mobile_admin.css"
          media="screen and (max-width: 767px)"
    /> 
    
    <!-- CSS link for IE browser version 9 and less -->
    <!--[if LTE IE 9]>
          <link rel="stylesheet"
          type="text/css"
          href="styles/desktop_admin.css"
          media="screen"
          />
    <![endif] --> 
    
    <!-- link to css file for printers -->
    <link rel="stylesheet"
          type="text/css"
          href="styles/print.css" 
          media="print" 
    />
    
    
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    
    <script>
      //function for search field
      $(document).ready(function () {
        $("#searchbox").keyup(function () { //when press a keyboard key
          var key = $(this).val(); //#searchbox value is assigned to a variable
          
          $.ajax({ //perform an async AJAX request
            url:'search.php', //send request to search.php
            type:'GET',  //method="get"
            data:'keyword='+key, //send in request keyword=[value of the input field]
            success:function (response) { // if success response returned
              $("#results").html(response); //inject into div "results"
            }//END success:function()
          });//END $.ajax()
        });//END $("#searchbox").keyup(function 
        
        $("#searchbox").blur(function () {//clear results when input in not focused
          $('#results').html('');
          //$('#results').empty(); //same as line above
        });
      });//END $(document).ready(function ()
    </script>
    
  </head>








  <body id="index">
   
    <div id="wrapper">
      
      
      
      <!-- ********************* START header + navigation ************************-->
      <div id="header_nav"> <!-- ***** #header_nav start *****  -->
        <header>
          <div id="logo"><a href="index.php" title="Home admin"><img src="../../images/logo.png" alt="coffeeccino" /></a></div>
          <div id="tagline"><a href="index.php" title="Home admin">Delight in every drop</a></div>
          <div id="user_section"><!-- LOGIN / LOGOUT nav menu-->
            <?php if(!isset($_SESSION['logged_admin'])) {
                echo '<span class="user_menu"><a href="login.php">LogIn</a></span>';
              } else {
                echo '<span class="user_menu"><a href="logout.php">LogOut</a></span>';
              }
            ?>
          </div><!-- END #user_section-->
        </header>

        <nav>
          <div id="menu">
            <a href="#" id="menulink" title="Menu"><!-- hamburger menu -->
              <span id="hamburger_top"></span>
              <span id="hamburger_middle"></span>
              <span id="hamburger_bottom"></span>
            </a>
            <ul id="navlist">
              <li><a href="index.php"
                     <?php if($active_page == 'index') {echo 'class="current"';}?>
                     title="Home Admin">Home Adm</a></li>
              <li><a href="product.php"
                     <?php if($active_page == 'product') {echo 'class="current"';}?>
                     title="Product" >Product</a></li>
              <li><a href="invoice.php"
                     <?php if($active_page == 'invoice') {echo 'class="current"';}?>
                     title="Invoice">Invoice</a></li>
              <li><a href="users.php"
                     <?php if($active_page == 'users') {echo 'class="current"';}?>
                     title="Users">Users</a></li>
              <li><a href="#"
                     <?php if($active_page == '#') {echo 'class="current"';}?>
                     title="Reserved Link">-</a></li>
            </ul>
          </div> <!-- end #menu -->
        </nav>
      </div><!-- ***** end header_nav ***** -->
      <!-- ********************* END header + navigation ************************-->




      <!-- ######################################################################-->
      <!-- /////////////////// START main content ///////////////////////////////-->
      <!-- ######################################################################-->
      <main id="content"> <!-- main content goes here -->
        
        
        
        <!--****************************START search**********************-->
        <div class="search">
          <form action="product_create.php" 
                id="search_form"
                method="get" 
                novalidate="novalidate" 
                autocomplete="off">
            <input type="text"
                   id="searchbox" 
                   name="keyword" 
                   maxlength="255" 
                   placeholder="Search product by name" />&nbsp; <!-- name="keyword" is used for $_GET['keyword'] -->
            <input id="search_button" type="submit" value="search" />
            <div id="results"></div><!-- live search results will appear here -->
          </form>
          <br />
          <div id="under_search_line"></div>
        </div><!--END div.search --> 
        <!--****************************END search**********************-->
        
        
        <h1 id="admin_msg">THIS IS THE ADMIN SITE!!!</h1>
        <h1><?=$title?></h1>
        
        
        <!-- if cant create a new product after submitting form, 
             display error message ($product_not_created)-->
        <?php if(isset($product_not_created)) :?>
          <h2><?php echo $product_not_created ?></h2>
        <?php endif; ?>
        
        
          <form method="post"
                action="product_create.php"
                id="product_add_form"
                name="product_add_form"
                accept-charset="utf-8" 
                novalidate="novalidate">
            
            <fieldset>
              <legend><b>New Product Info</b></legend>
              
              <p>
                <label for="title">Title</label>
                <input type="text"
                       id="title"
                       name="title"
                       maxlength="255"
                       value="<?php 
                                if(!empty($_POST['title'])){
                                  echo esc ($_POST['title']);}
                              ?>"
                       placeholder="product name" /><br />
                       <!-- value="<?php//...?>" is for sticky input field -->
                
                <!-- display error message near the field -->
                <?php if(!empty($errors['title'])) : ?>
                  <span class="error"><?=$errors['title']?></span><br />
                <?php endif; ?>
              </p>

              <p>
                <label for="image">Image</label>
                <input type="text"
                       id="image"
                       name="image"
                       maxlength="255"
                       value="<?php 
                                if(!empty($_POST['image'])){
                                  echo esc ($_POST['image']);}
                              ?>"
                       placeholder="image name. ex.: title.jpg" /><br />
                       
                <?php if(!empty($errors['image'])) : ?>
                  <span class="error"><?=$errors['image']?></span><br />
                <?php endif; ?>
              </p>

              <p>
                <label for="price">Price</label>
                <input type="text"
                       id="price"
                       name="price"
                       maxlength="255"
                       value="<?php 
                                if(isset($_POST['price'])){
                                  echo esc ($_POST['price']);}
                              ?>"
                       placeholder="ex. 22.33" /><br />
                       
                <?php if(!empty($errors['price'])) : ?>
                  <span class="error"><?=$errors['price']?></span><br />
                <?php endif; ?>
              </p>

              <p>
                <label for="description">Description</label>
                <!-- textarea line MUST be in 1 line and no spaces between tags,
                     otherwise default spaces will be in the field(it will not be empty) -->
                <textarea name="description" form="product_add_form"><?php if(!empty($_POST['description'])){echo esc ($_POST['description']);} ?></textarea>
                
                <?php if(!empty($errors['description'])) : ?>
                  <span class="error"><?=$errors['description']?></span><br />
                <?php endif; ?>
              </p>

              <p>
                <label for="category">Category</label>
                <input type="text"
                       id="category"
                       name="category"
                       maxlength="255"
                       value="<?php 
                                if(!empty($_POST['category'])){
                                  echo esc ($_POST['category']);}
                              ?>"
                       placeholder="Bag OR Canister OR K-cup" /><br />
                       
                <?php if(!empty($errors['category'])) : ?>
                  <span class="error"><?=$errors['category']?></span><br />
                <?php endif; ?>
              </p>

              <p>
                <label for="in_stock">In stock</label>
                <input type="text"
                       id="in_stock"
                       name="in_stock"
                       maxlength="255"
                       value="<?php 
                                if(isset($_POST['in_stock'])){
                                  echo esc ($_POST['in_stock']);}
                              ?>"
                       placeholder="0 or 1" /><br />
                       
                <?php if(!empty($errors['in_stock'])) : ?>
                  <span class="error"><?=$errors['in_stock']?></span><br />
                <?php endif; ?>
              </p>

              <p>
                <label for="quantity_available">Quantity available</label>
                <input type="text"
                       id="quantity_available"
                       name="quantity_available"
                       maxlength="255"
                       value="<?php 
                                if(isset($_POST['quantity_available'])){
                                  echo esc ($_POST['quantity_available']);}
                              ?>"
                       placeholder="ex. 254" /><br /><!-- isset allows to enter 0 (vs !empty) -->
                       
                <?php if(!empty($errors['quantity_available'])) : ?>
                  <span class="error"><?=$errors['quantity_available']?></span><br />
                <?php endif; ?>
              </p>

              <p>
                <label for="supplier">Supplier</label>
                <input type="text"
                       id="supplier"
                       name="supplier"
                       maxlength="255"
                       value="<?php 
                                if(!empty($_POST['supplier'])){
                                  echo esc ($_POST['supplier']);}
                              ?>"
                       placeholder="ex. coffee ltd, roast ltd, bean ltd" /><br />
                       
                <?php if(!empty($errors['supplier'])) : ?>
                  <span class="error"><?=$errors['supplier']?></span><br />
                <?php endif; ?>
              </p>

              <p>
                <label for="grade">Grade</label>
                <input type="text"
                       id="grade"
                       name="grade"
                       maxlength="255"
                       value="<?php 
                                if(!empty($_POST['grade'])){
                                  echo esc ($_POST['grade']);}
                              ?>"
                       placeholder="A or B or C" /><br />
                       
                <?php if(!empty($errors['grade'])) : ?>
                  <span class="error"><?=$errors['grade']?></span><br />
                <?php endif; ?>
              </p>

              <p>
                <label for="rating">Rating</label>
                <input type="text"
                       id="rating"
                       name="rating"
                       maxlength="255"
                       value="<?php 
                                if(!empty($_POST['rating'])){
                                  echo esc ($_POST['rating']);}
                              ?>"
                       placeholder="1 or 2 or 3 or 4 or 5" /><br />
                       
                <?php if(!empty($errors['rating'])) : ?>
                  <span class="error"><?=$errors['rating']?></span><br />
                <?php endif; ?>
              </p>

              <p>
                <label for="deleted">Deleted</label>
                <input type="text"
                       id="deleted"
                       name="deleted"
                       maxlength="255"
                       value="<?php 
                                if(isset($_POST['deleted'])){
                                  echo esc ($_POST['deleted']);}
                              ?>"
                       placeholder="0 or 1" /><br />
                       
                <?php if(!empty($errors['deleted'])) : ?>
                  <span class="error"><?=$errors['deleted']?></span><br />
                <?php endif; ?>
              </p>
              
            </fieldset>
          
            <p id="form_submit_buttons">
              <input type="submit"
                     value="Save"
                     class="button" />&nbsp; &nbsp;
            </p>
          
          </form>
        
      </main>
      <!-- ######################################################################-->
      <!-- /////////////////// END main content ///////////////////////////////-->
      <!-- ######################################################################-->
      
      
      
      <footer>
        <h2>***FOOTER***</h2>
      </footer>
      
    </div> <!-- end #wrapper -->
    
  </body>
  
</html>