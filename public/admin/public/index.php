<?php

/**
* ------ BACKEND! ------
* 
* Capstone Project PHP
* @file admin/public/index.php
* @author Alex Ten
* created_at 2018-09-11
**/

require __DIR__ . '/../config_admin.php'; //main config file
require '../functions_admin.php';  //main functions file



$title = 'Home (Admin)';
$active_page = 'index';


if(!isset($_SESSION['logged_admin'])){
  header ('Location: login.php');
  die;
}


//get number of products in table 'product_coffee'
$num_products = getProductCount ($dbh);
//var_dump($num_products);
$num = $num_products['COUNT(*)'];

//get average price of products in table 'product_coffee'
$avg_product_price = getAvgPrice ($dbh);
//var_dump($avg_product_price);
$avg = $avg_product_price['AVG(product_coffee.price)'];
$avg = round($avg,2); //round number to 2 digits after .(point)

//get minimum price of relevant products in table 'product_coffee'
$min__relevant_products_price = getMinPrice ($dbh);
//var_dump($min__relevant_products_price);
$min = $min__relevant_products_price['MIN(product_coffee.price)'];

//get maximum price of deleted products in table 'product_coffee'
$max_deleted_products_price = getMaxPrice ($dbh);
//var_dump($max_deleted_products_price);
$max = $max_deleted_products_price['MAX(product_coffee.price)'];


?><!doctype html>

<html lang="en">

  <head>
    
    <title><?php echo $title; ?></title>
    <meta charset="utf-8" />
    <meta name="viewport"
          content="width=device-width, initial-scale=1" />
          
    <link rel="shortcut icon" href="../../images/favicon64.png" type="image/png" /><!-- favorite icon in title link -->
    
    <!-- link to css file for desktops -->
    <link rel="stylesheet"
          type="text/css"
          href="styles/desktop_admin.css"
          media="screen and (min-width: 768px)"
    />
    
    <!-- link to css file for mobiles -->
    <link rel="stylesheet"
          type="text/css"
          href="styles/mobile_admin.css"
          media="screen and (max-width: 767px)"
    /> 
    
    <!-- CSS link for IE browser version 9 and less -->
    <!--[if LTE IE 9]>
          <link rel="stylesheet"
          type="text/css"
          href="styles/desktop_admin.css"
          media="screen"
          />
    <![endif] --> 
    
    <!-- link to css file for printers -->
    <link rel="stylesheet"
          type="text/css"
          href="styles/print.css" 
          media="print" 
    />
    
  </head>








  <body id="index">
   
    <div id="wrapper">
      
      
      
      <!-- ********************* START header + navigation ************************-->
      <div id="header_nav"> <!-- ***** #header_nav start *****  -->
        <header>
          <div id="logo"><a href="index.php" title="Home admin"><img src="../../images/logo.png" alt="coffeeccino" /></a></div>
          <div id="tagline"><a href="index.php" title="Home admin">Delight in every drop</a></div>
          <div id="user_section"><!-- LOGIN / LOGOUT nav menu-->
            <?php if(!isset($_SESSION['logged_admin'])) {
                echo '<span class="user_menu"><a href="login.php">LogIn</a></span>';
              } else {
                echo '<span class="user_menu"><a href="logout.php">LogOut</a></span>';
              }
            ?>
          </div><!-- END #user_section-->
        </header>

        <nav>
          <div id="menu">
            <a href="#" id="menulink" title="Menu"><!-- hamburger menu -->
              <span id="hamburger_top"></span>
              <span id="hamburger_middle"></span>
              <span id="hamburger_bottom"></span>
            </a>
            <ul id="navlist">
              <li><a href="index.php"
                     <?php if($active_page == 'index') {echo 'class="current"';}?>
                     title="Home Admin">Home Adm</a></li>
              <li><a href="product.php"
                     <?php if($active_page == 'product') {echo 'class="current"';}?>
                     title="Product" >Product</a></li>
              <li><a href="invoice.php"
                     <?php if($active_page == 'invoice') {echo 'class="current"';}?>
                     title="Invoice">Invoice</a></li>
              <li><a href="users.php"
                     <?php if($active_page == 'users') {echo 'class="current"';}?>
                     title="Users">Users</a></li>
              <li><a href="#"
                     <?php if($active_page == '#') {echo 'class="current"';}?>
                     title="Reserved Link">-</a></li>
            </ul>
          </div> <!-- end #menu -->
        </nav>
      </div><!-- ***** end header_nav ***** -->
      <!-- ********************* END header + navigation ************************-->




      <!-- ######################################################################-->
      <!-- /////////////////// START main content ///////////////////////////////-->
      <!-- ######################################################################-->
      <main id="content"> <!-- main content goes here -->
        
        
        <h1 id="admin_msg">THIS IS THE ADMIN SITE !!!</h1>
        <h1><?=$title?></h1>
        
        
        <p>
          <strong>Number of products (deleted = 0) in 'product_coffee' table: </strong>
          <span class="number"><?php echo $num ?></span>
        </p>
        
        <p>
          <strong>Average price of products (deleted = 0) in 'product_coffee' table: </strong>
          <span class="number">$<?php echo $avg ?></span>
        </p>
        
        <p>
          <strong>Minimum price products (deleted = 0) in 'product_coffee' table: </strong>
          <span class="number">$<?php echo $min ?></span>
        </p>
        
        <p>
          <strong>Maximum price of deleted products (deleted = 1) in 'product_coffee' table: </strong>
          <span class="number">$<?php echo $max ?></span>
        </p>
        
        
      </main>
      <!-- ######################################################################-->
      <!-- /////////////////// END main content ///////////////////////////////-->
      <!-- ######################################################################-->
      
      
      
      
      
      <footer>
        <h2>***FOOTER***</h2>
      </footer>
      
    </div> <!-- end #wrapper -->
    
  </body>
  
</html>