<?php

/**
* ------ BACKEND! ------
* 
* Capstone Project PHP
* @file admin/public/product_delete.php
* @author Alex Ten
* created_at 2018-09-11
**/

require __DIR__ . '/../config_admin.php';

if(!isset($_SESSION['logged_admin'])){
  header ('Location: login.php');
  die;
}



//if this page receives $_GET['product_id'] sent by product.php
if(!empty($_GET['product_id'])) {
  //replace value of field deleted for product identified by id
  $query = "UPDATE
            product_coffee
            SET
            deleted = 1
            WHERE
            product_id = :product_id
           ";

  $stmt = $dbh->prepare($query);
  $stmt->bindValue(':product_id', $_GET['product_id'], PDO::PARAM_INT);
  $stmt->execute();
  header ('Location: product.php');
  die;
}
//if this page doesnt receive $_GET['product_id'] sent by product.php
else {
  header ('Location: product.php');
  die;
}


