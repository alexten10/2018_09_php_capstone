<?php

/**
* Capstone Project PHP
* @file login.php
* @author Alex Ten
* created_at 2018-09-05
**/

require __DIR__ . '/../config_admin.php'; //main config file
require '../functions_admin.php';  //main functions file

use \Classes\Utility\ValidatorAdmin;
$vldtr = new ValidatorAdmin;

$title = 'LogIn';

$active_page = 'login';

//var_dump($_SESSION); //to see what data comes first in $_SESSION array


//testing the $_POST (if have POST)
if($_SERVER['REQUEST_METHOD'] == 'POST') {
  $vldtr->validateEmail('email');
  $vldtr->required('email');
  
  $vldtr->required('password');
  
  
  //if no errors found
  if(empty($vldtr->errors())) {
    $admin_info = getAdminByEmail($dbh, $_POST['email']);
    var_dump($admin_info); 
    
    //compare typed password with the password stored in database
    if(password_verify($_POST['password'], $admin_info['password'])) {
      $greeting = "Welcome, {$admin_info['first_name']} {$admin_info['last_name']}!";
      $_SESSION['success'] = $greeting;//if email and password match, set $_SESSION['success'] to exist
      $_SESSION['logged_admin'] = true;
      session_regenerate_id();
      header('Location: product.php');//redirect to profile.php
      die;
    }//END if
    
    else {//if passwords dont match OR no info found in database about user by email
      $_SESSION['no_success'] = 'Sorry, credentials don\'t match!';
      $flash_message_no_success = $_SESSION['no_success'];
      unset($_SESSION['no_success']);
    }//END else
    
  }//END if(empty($vldtr->errors())) 
  
}//END if($_SERVER['REQUEST_METHOD'] == 'POST')


//if $_SESSION['logged_out'] exists, which comes form logout.php
if(isset($_SESSION['logged_out'])){
  $_SESSION['logged_out'] = 'You have logged out.';
  $flash_message_logout = $_SESSION['logged_out'];
  unset ($_SESSION['logged_out']);
}


$errors = $vldtr->errors();

?><!doctype html>

<html lang="en">

  <head>
    
    <title><?php echo $title; ?></title>
    <meta charset="utf-8" />
    <meta name="viewport"
          content="width=device-width, initial-scale=1" />
          
    <link rel="shortcut icon" href="../../images/favicon64.png" type="image/png" /><!-- favorite icon in title link -->
    
    <!-- link to css file for desktops -->
    <link rel="stylesheet"
          type="text/css"
          href="styles/desktop_admin.css"
          media="screen and (min-width: 768px)"
    />
    
    <!-- link to css file for mobiles -->
    <link rel="stylesheet"
          type="text/css"
          href="styles/mobile_admin.css"
          media="screen and (max-width: 767px)"
    /> 
    
    <!-- CSS link for IE browser version 9 and less -->
    <!--[if LTE IE 9]>
          <link rel="stylesheet"
          type="text/css"
          href="styles/desktop_admin.css"
          media="screen"
          />
    <![endif] --> 
    
    <!-- link to css file for printers -->
    <link rel="stylesheet"
          type="text/css"
          href="styles/print.css" 
          media="print" 
    />
    
  </head>




  <body id="index">

    <div id="wrapper">
    
      <!-- ********************* START header + navigation ************************-->
      <div id="header_nav"> <!-- ***** #header_nav start *****  -->
        <header>
          <div id="logo"><a href="index.php" title="Home admin"><img src="../../images/logo.png" alt="coffeeccino" /></a></div>
          <div id="tagline"><a href="index.php" title="Home admin">Delight in every drop</a></div>
          <div id="user_section"><!-- LOGIN / LOGOUT nav menu-->
            <?php if(!isset($_SESSION['logged_admin'])) {
                echo '<span class="user_menu"><a href="login.php">LogIn</a></span>';
              } else {
                echo '<span class="user_menu"><a href="logout.php">LogOut</a></span>';
              }
            ?>
          </div><!-- END #user_section-->
        </header>

        <nav>
          <div id="menu">
            <a href="#" id="menulink" title="Menu"><!-- hamburger menu -->
              <span id="hamburger_top"></span>
              <span id="hamburger_middle"></span>
              <span id="hamburger_bottom"></span>
            </a>
            <ul id="navlist">
              <li><a href="index.php"
                     <?php if($active_page == 'index') {echo 'class="current"';}?>
                     title="Home Admin">Home Adm</a></li>
              <li><a href="product.php"
                     <?php if($active_page == 'product') {echo 'class="current"';}?>
                     title="Product" >Product</a></li>
              <li><a href="invoice.php"
                     <?php if($active_page == 'invoice') {echo 'class="current"';}?>
                     title="Invoice">Invoice</a></li>
              <li><a href="users.php"
                     <?php if($active_page == 'users') {echo 'class="current"';}?>
                     title="Users">Users</a></li>
              <li><a href="#"
                     <?php if($active_page == '#') {echo 'class="current"';}?>
                     title="Reserved Link">-</a></li>
            </ul>
          </div> <!-- end #menu -->
        </nav>
      </div><!-- ***** end header_nav ***** -->
      <!-- ********************* END header + navigation ************************-->

      
      <main id="content"> <!-- main content goes here -->
        <h1 id="admin_msg">THIS IS THE ADMIN SITE!!!</h1>
        <h1 id="must_login">You must be logged in to use this site !</h1>
        
        <h1><?=$title?></h1>
        
        <!-- show error flash message when login is not successful -->
        <?php if(!empty($flash_message_no_success)) echo "<h2 id=\"flash_message_no_success\">$flash_message_no_success</h2>"; ?><!--show message for unsuccessful log in-->
        
        <?php if(!empty($flash_message_logout)) echo "<h2 id=\"flash_message_success\">$flash_message_logout</h2>"; ?> <!--show message for success log out, css is for id="falsh_message_success"-->
        
          <form method="post"
                action="login.php"
                id="login"
                name="login"
                accept-charset="utf-8" 
                novalidate="novalidate">
            
            <fieldset>
              <legend><b>LogIn</b></legend>
              
              <p>
                <label for="email">Email Address</label>
                <input type="email"
                       name="email"
                       id="email"
                       value="<?php 
                                if (!empty($_POST['email'])) {
                                  echo esc ($_POST['email']);
                                }
                              ?>"
                       placeholder="Enter your registered email" /><br />
                       
                       <?php if(!empty($errors['email'])) : ?>
                        <span class="error"><?=$errors['email']?></span><br />
                      <?php endif; ?>
              </p>

              <p>
                <label for="pass">Password</label>
                <input type="password"
                       id="pass"
                       name="password"
                       maxlength="255"
                       value=""
                       placeholder="Enter your password" /><br />
                       
                <?php if(!empty($errors['password'])) : ?>
                  <span class="error"><?=$errors['password']?></span><br />
                <?php endif; ?>
              </p>
              
            </fieldset>
          
          
            <p id="form_submit_buttons">
              <input type="submit" value="Login" class="button" />
            </p>
          
          </form>
        
        
      </main> <!-- end main#content -->
      
      <footer>
        <h2>***FOOTER is here***</h2>
      </footer>
    
    </div> <!-- end #wrapper -->
    
  </body>
  
</html>