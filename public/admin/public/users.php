<?php

/**
* ------ BACKEND! ------
* 
* Capstone Project PHP
* @file admin/public/users.php
* @author Alex Ten
* created_at 2018-09-11
**/

require __DIR__ . '/../config_admin.php';

$title = 'Users';
$active_page = 'users';

if(!isset($_SESSION['logged_admin'])){
  header ('Location: login.php');
  die;
}

?><!doctype html>

<html lang="en">

  <head>
    
    <title><?php echo $title; ?></title>
    <meta charset="utf-8" />
    <meta name="viewport"
          content="width=device-width, initial-scale=1" />
          
    <link rel="shortcut icon" href="../../images/favicon64.png" type="image/png" /><!-- favorite icon in title link -->
    
    <!-- link to css file for desktops -->
    <link rel="stylesheet"
          type="text/css"
          href="styles/desktop_admin.css"
          media="screen and (min-width: 768px)"
    />
    
    <!-- link to css file for mobiles -->
    <link rel="stylesheet"
          type="text/css"
          href="styles/mobile_admin.css"
          media="screen and (max-width: 767px)"
    /> 
    
    <!-- CSS link for IE browser version 9 and less -->
    <!--[if LTE IE 9]>
          <link rel="stylesheet"
          type="text/css"
          href="styles/desktop_admin.css"
          media="screen"
          />
    <![endif] --> 
    
    <!-- link to css file for printers -->
    <link rel="stylesheet"
          type="text/css"
          href="styles/print.css" 
          media="print" 
    />
    
    
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    
    <script>
      //function for search field
      $(document).ready(function () {
        $("#searchbox").keyup(function () { //when press a keyboard key
          var key = $(this).val(); //#searchbox value is assigned to a variable
          
          $.ajax({ //perform an async AJAX request
            url:'search.php', //send request to search.php
            type:'GET',  //method="get"
            data:'keyword='+key, //send in request keyword=[value of the input field]
            success:function (response) { // if success response returned
              $("#results").html(response); //inject into div "results"
            }//END success:function()
          });//END $.ajax()
        });//END $("#searchbox").keyup(function 
        
        $("#searchbox").blur(function () {//clear results when input in not focused
          $('#results').html('');
          //$('#results').empty(); //same as line above
        });
      });//END $(document).ready(function ()
    </script>
    
  </head>








  <body id="index">
   
    <div id="wrapper">
      
      
      
      <!-- ********************* START header + navigation ************************-->
      <div id="header_nav"> <!-- ***** #header_nav start *****  -->
        <header>
          <div id="logo"><a href="index.php" title="Home admin"><img src="../../images/logo.png" alt="coffeeccino" /></a></div>
          <div id="tagline"><a href="index.php" title="Home admin">Delight in every drop</a></div>
          <div id="user_section"><!-- LOGIN / LOGOUT nav menu-->
            <?php if(!isset($_SESSION['logged_admin'])) {
                echo '<span class="user_menu"><a href="login.php">LogIn</a></span>';
              } else {
                echo '<span class="user_menu"><a href="logout.php">LogOut</a></span>';
              }
            ?>
          </div><!-- END #user_section-->
        </header>

        <nav>
          <div id="menu">
            <a href="#" id="menulink" title="Menu"><!-- hamburger menu -->
              <span id="hamburger_top"></span>
              <span id="hamburger_middle"></span>
              <span id="hamburger_bottom"></span>
            </a>
            <ul id="navlist">
              <li><a href="index.php"
                     <?php if($active_page == 'index') {echo 'class="current"';}?>
                     title="Home Admin">Home Adm</a></li>
              <li><a href="product.php"
                     <?php if($active_page == 'product') {echo 'class="current"';}?>
                     title="Product" >Product</a></li>
              <li><a href="invoice.php"
                     <?php if($active_page == 'invoice') {echo 'class="current"';}?>
                     title="Invoice">Invoice</a></li>
              <li><a href="users.php"
                     <?php if($active_page == 'users') {echo 'class="current"';}?>
                     title="Users">Users</a></li>
              <li><a href="#"
                     <?php if($active_page == '#') {echo 'class="current"';}?>
                     title="Reserved Link">-</a></li>
            </ul>
          </div> <!-- end #menu -->
        </nav>
      </div><!-- ***** end header_nav ***** -->
      <!-- ********************* END header + navigation ************************-->




      <!-- ######################################################################-->
      <!-- /////////////////// START main content ///////////////////////////////-->
      <!-- ######################################################################-->
      <main id="content"> <!-- main content goes here -->
        
        
        
        <!--****************************START search**********************-->
        <div class="search">
          <form action="users.php" 
                id="search_form"
                method="get" 
                novalidate="novalidate" 
                autocomplete="off">
            <input type="text"
                   id="searchbox" 
                   name="keyword" 
                   maxlength="255" 
                   placeholder="Search product by name" />&nbsp; <!-- name="keyword" is used for $_GET['keyword'] -->
            <input id="search_button" type="submit" value="search" />
            <div id="results"></div><!-- live search results will appear here -->
          </form>
          <br />
          <div id="under_search_line"></div>
        </div><!--END div.search --> 
        <!--****************************END search**********************-->
        
        
        <h1 id="admin_msg">THIS IS THE ADMIN SITE!!!</h1>
        <h1><?=$title?></h1>
        
        
        
        
        
        
        
        
      </main>
      <!-- ######################################################################-->
      <!-- /////////////////// END main content ///////////////////////////////-->
      <!-- ######################################################################-->
      
      
      
      <footer>
        <h2>***FOOTER***</h2>
      </footer>
      
    </div> <!-- end #wrapper -->
    
  </body>
  
</html>