  <?php
  
/**
* ------ BACKEND! ------
* 
* Capstone Project PHP
* @file search.php
* @author Alex Ten
* created_at 2018-09-11
**/
  
  
  require __DIR__ . '/../config_admin.php'; //main config file
  require '../functions_admin.php';  //main functions file
  
  
  //for search field, if we get any keyword value in $_GET
  if(!empty($_GET['keyword'])) {
    $keyword = strip_tags(htmlspecialchars($_GET['keyword'])); //escaping special chars and removing tags for security reasons
    $keyword="%$keyword%"; //search including any chars before typed keyword (%$keyword) any chars after typed ($keyword%)
    
    $result = getLiveSearchResults ($dbh, $keyword);
    
    //print_r ($result); //to check if correct results appear in array
    if(empty($result)) { // so if we have 0 records associated to keyword display no records found
      echo '<div id="item">No results found!</div>';
    }//END if(empty($result))
    else {
      foreach($result as $row => $innerArray) { //targeting arrays inside $result array //in this case $innerArray is an array inside $result array
        echo "<a href='product.php?product_id={$innerArray['product_id']}'> {$innerArray['title']} </a><br />";
      }// END foreach
    }; //END else
  }; //END if(!empty($_GET['keyword']))