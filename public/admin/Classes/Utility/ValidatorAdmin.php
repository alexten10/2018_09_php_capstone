<?php

/**
* Capstone Project PHP
* @file ValidatorAdmin.php
* @author Alex Ten
* created_at 2018-09-05
**/


namespace Classes\Utility;

//file name and class name must be same, otherwise error
class ValidatorAdmin
{
  // our errors array
  private $errors = [];
  
  
  /**
  *validate required fields in $_POST
  *@param String $field_name - name of field to validate
  */
  public function required($field_name)
  {
    $label = ucfirst(str_replace('_', ' ', $field_name));
    if(empty($_POST[$field_name])) {
      $this->errors[$field_name] = "'{$label}' is a required field";
    }//END if
  }//END function required()
  
  
  /**
  *validate required fields in $_POST
  *@param String $field_name - name of field to validate
  */
  public function requiredNumeric($field_name)
  {
    $label = ucfirst(str_replace('_', ' ', $field_name));
    if(!is_numeric($_POST[$field_name])) {
      $this->errors[$field_name] = "'{$label}' is a required numeric field";
    }//END if
  }//END function required()
  
  
  /**
  *validate fields in $_POST for general characters
  *@param String $field_name - name of field to validate
  */
  public function validateForGeneralRules($field_name)
  {
    $label = ucfirst(str_replace('_', ' ', $field_name));
    $pattern = '/^[A-z0-9\,\.\-\s]*$/';
    if(preg_match($pattern, $_POST[$field_name], $matches) === 0) { //if no matches found
      $this->errors[$field_name] = "'{$label}' field can have only letters, numbers, dashes, commas, periods, and spaces ";
    }//END if
  }//END function validateForGeneralRules()
  
  
  /**
  *validate image name in $_POST for allowed characters
  *@param String $field_name - name of field to validate
  */
  public function validateImage($field_name)
  {
    $label = ucfirst(str_replace('_', ' ', $field_name));
    $pattern = '/^[A-z0-9\.\_]*$/';
    if(preg_match($pattern, $_POST[$field_name], $matches) === 0) { //if no matches found
      $this->errors[$field_name] = "'{$label}' field can have only letters, numbers, underscores, and periods. ex.: coffee.jpg";
    }//END if
  }//END function validateImage()
  
  
  /**
  *validate price in $_POST for allowed characters
  *@param String $field_name - name of field to validate
  */
  public function validatePrice($field_name)
  {
    $label = ucfirst(str_replace('_', ' ', $field_name));
    $pattern = '/^[0-9]+(\.[0-9]{2})?$/';
    if(preg_match($pattern, $_POST[$field_name], $matches) === 0) { //if no matches found
      $this->errors[$field_name] = "'{$label}' field can have only price format numbers. ex.: 2.99";
    }//END if
  }//END function validatePrice()
  
  
  /**
  *validate category in $_POST for allowed characters
  *@param String $field_name - name of field to validate
  */
  public function validateCategory($field_name)
  {
    $label = ucfirst(str_replace('_', ' ', $field_name));
    $pattern = '/^[A-z0-9\-]*$/';
    if(preg_match($pattern, $_POST[$field_name], $matches) === 0) { //if no matches found
      $this->errors[$field_name] = "'{$label}' field can have only letters, numbers, and dashes. ex.: K-cup";
    }//END if
  }//END function validateCategory()
  
  
  /**
  *validate boolean in $_POST for allowed characters
  *@param String $field_name - name of field to validate
  */
  public function validateBoolean($field_name)
  {
    $label = ucfirst(str_replace('_', ' ', $field_name));
    $pattern = '/^([0]||[1]){1}$/';
    if(preg_match($pattern, $_POST[$field_name], $matches) === 0) { //if no matches found
      $this->errors[$field_name] = "'{$label}' field can have only one digit: 0 or 1";
    }//END if
  }//END function validateBoolean()
  

  /**
  *validate field in $_POST for allowed digits
  *@param String $field_name - name of field to validate
  */
  public function validateForDigits($field_name)
  {
    $label = ucfirst(str_replace('_', ' ', $field_name));
    $pattern = '/^([0-9]+)$/';
    if(preg_match($pattern, $_POST[$field_name], $matches) === 0) { //if no matches found
      $this->errors[$field_name] = "'{$label}' field can have only digits";
    }//END if
  }//END function validateForDigits()
  
  
  /**
  *validate Grade in $_POST for allowed characters
  *@param String $field_name - name of field to validate
  */
  public function validateGrade($field_name)
  {
    $label = ucfirst(str_replace('_', ' ', $field_name));
    $pattern = '/^([A]||[B]||[C]){1}$/';
    if(preg_match($pattern, $_POST[$field_name], $matches) === 0) { //if no matches found
      $this->errors[$field_name] = "'{$label}' field can have only one capital letter: A or B or C";
    }//END if
  }//END function validateGrade()
  
  
  /**
  *validate rating in $_POST for allowed characters
  *@param String $field_name - name of field to validate
  */
  public function validateRating($field_name)
  {
    $label = ucfirst(str_replace('_', ' ', $field_name));
    $pattern = '/^([1]||[2]||[3]||[4]||[5]){1}$/';
    if(preg_match($pattern, $_POST[$field_name], $matches) === 0) { //if no matches found
      $this->errors[$field_name] = "'{$label}' field can have only one digit: 1 or 2 or 3 or 4 or 5";
    }//END if
  }//END function validateRating()
  
  
  /**
  *validate email field in $_POST for proper format
  *@param String $field_name - name of field to validate
  */
  public function validateEmail($field_name)
  {
    if(!filter_var(($_POST[$field_name]), FILTER_VALIDATE_EMAIL)) {
      $this->errors[$field_name] = "Please enter a valid email address";
    }//END if
  }//END function validateEmail()
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  /**
  *@return Array errors - array of error messages
  */
  public function errors()
  {
    return $this->errors;
  }//END function errors()
  
  
}//END class ValidatorAdmin

























