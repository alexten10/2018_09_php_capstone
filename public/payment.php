<?php

/**
* Capstone Project PHP
* @file payment.php
* @author Alex Ten
* created_at 2018-09-10
**/

require __DIR__ . '/../config.php';
require '../functions.php';
use \Classes\Utility\Validator;
$vldtr = new Validator;
//var_dump($vldtr); //checking if $vldtr exists

$title = 'Payment';

$active_page = 'payment';


//var_dump($_SESSION);

//after checkout $_SESSION['total'] must exist
//this prevents of displaying this page (payment.php) if a user types address of this page manually
if(!isset($_SESSION['total'])) {
  header ('Location:shop_coffee.php');
  die;
}

//only logged users are allowed to see this page
if(!isset($_SESSION['logged_in'])) {
  header ('Location:shop_coffee.php');
  die;
}


if($_SERVER['REQUEST_METHOD'] == 'POST') {
  
  // !!! order of calling functions is important !!!
  
  $vldtr->validateCreditCard('card_number');
  $vldtr->required('card_number');
  
  $vldtr->validateForSymbols('card_holder');
  $vldtr->required('card_holder');
  
  $vldtr->validateCardExpiry('card_expiry');
  $vldtr->required('card_expiry');
  
  $vldtr->validateCvv('cvv');
  $vldtr->required('cvv');
  
  //if no errors found
  if(empty($vldtr->errors())) {
    $_SESSION['card_number'] = $_POST;
    header('Location:invoice.php');
    die;
  }//END if(empty($vldtr->errors()))

}//END if($_SERVER['REQUEST_METHOD'] == 'POST')

$errors = $vldtr->errors();

?>

<!-- <head> -->
<?php include '../includes/head.inc.php' ?>


  <body id="index">

    <div id="wrapper">
    
      <!-- header with navigation -->
      <?php include '../includes/header.inc.php' ?>
      
      <main id="content"> <!-- main content goes here -->
        
        <?php include '../includes/search.inc.php' ?>
        
        <h1><?=$title?></h1>
        
        
          
        <form method="post"
              action="payment.php"
              id="payment_form"
              name="payment_form"
              accept-charset="utf-8" 
              novalidate="novalidate">
          
          <fieldset>
            <legend><b>Payment Info</b></legend>
            
            <p>
              <label for="card_number">Card Number</label>
              <input type="text"
                     id="card_number"
                     name="card_number"
                     maxlength="20"
                     value="<?php 
                              if(!empty($_POST['card_number'])){
                                echo esc ($_POST['card_number']);}
                            ?>"
                     placeholder="Type your 16 digit card number" /><br />
                     
              <?php if(!empty($errors['card_number'])) : ?>
                <span class="error"><?=$errors['card_number']?></span><br />
              <?php endif; ?>
            </p>

            <p>
              <label for="card_holder">Cardholder Name</label>
              <input type="text"
                     id="card_holder"
                     name="card_holder"
                     maxlength="150"
                     value="<?php 
                              if(!empty($_POST['card_holder'])){
                                echo esc ($_POST['card_holder']);}
                            ?>"
                     placeholder="Type cardholder name" /><br />
                     
              <?php if(!empty($errors['card_holder'])) : ?>
                <span class="error"><?=$errors['card_holder']?></span><br />
              <?php endif; ?>
            </p>
            
            <p>
              <label for="card_expiry">Card Expiry Date</label>
              <input type="text"
                     id="card_expiry"
                     name="card_expiry"
                     maxlength="20"
                     value="<?php 
                              if(!empty($_POST['card_expiry'])){
                                echo esc ($_POST['card_expiry']);}
                            ?>"
                     placeholder="MMYY" /><br /><br />
                     
              <?php if(!empty($errors['card_expiry'])) : ?>
                <span class="error"><?=$errors['card_expiry']?></span><br />
              <?php endif; ?>
            </p>
            
            <p>
              <label for="cvv">CVV</label>
              <input type="text"
                     id="cvv"
                     name="cvv"
                     maxlength="20"
                     value="<?php 
                              if(!empty($_POST['cvv'])){
                                echo esc ($_POST['cvv']);}
                            ?>"
                     placeholder="Type 3 digit CVV" /><br />
                    
              <?php if(!empty($errors['cvv'])) : ?>
                <span class="error"><?=$errors['cvv']?></span><br />
              <?php endif; ?>
            </p>
            
            <h2>Amount: $<?php echo $_SESSION['total'] ?></h2>
            
          </fieldset>
        
          <p id="form_submit_buttons">
            <input type="submit"
                   value="Pay"
                   class="button" />&nbsp; &nbsp;
          </p>
          
        </form>
        
      </main> <!-- end main#content -->
      
      <!-- footer -->
      <?php include '../includes/footer.inc.php' ?>
    
    </div> <!-- end #wrapper -->
    
  </body>
  
</html>