<?php

/**
* Capstone Project PHP
* @file contact.php
* @author Alex Ten
* created_at 2018-09-05
**/

require __DIR__ . '/../config.php';

$title = 'Contact Us';

$active_page = 'contact';

?>

<!-- <head> -->
<?php include '../includes/head.inc.php' ?>


  <body id="index">

    <div id="wrapper">
    
      <!-- header with navigation -->
      <?php include '../includes/header.inc.php' ?>
      
      <main id="content"> <!-- main content goes here -->
        
        <?php include '../includes/search.inc.php' ?>
        
        <?php if(isset($_SESSION['cart'])) { //if anything is added in cart
          include '../includes/cart.inc.php';
        } ?>
        
        <h1><?=$title?></h1>
        <br/>
        
        <div id="contact">
          <h2>Our address:</h2>
          <p>11111 Portage Avenue, Winnipeg, Manitoba, Canada, Z1X1C1</p>
          <br />
          <p><strong>Phone:</strong> +1 204 000-2233</p>
          <p><strong>Email:</strong> office@coffeccino.com</p>
        <div> <!-- END #contact -->
          
      </main> <!-- end main#content -->
      
      <!-- footer -->
      <?php include '../includes/footer.inc.php' ?>
    
    </div> <!-- end #wrapper -->
    
  </body>
  
</html>