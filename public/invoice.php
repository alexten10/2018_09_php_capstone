<?php

/**
* Capstone Project PHP
* @file invoice.php
* @author Alex Ten
* created_at 2018-09-11
**/
 
require __DIR__ . '/../config.php'; //main config file
require '../functions.php';  //main functions file

$title = 'Invoice';

$active_page = 'invoice';

//var_dump($_POST);
//var_dump($_SESSION);

//only logged users are allowed to see this page
if(!isset($_SESSION['logged_in'])) {
  header ('Location:shop_coffee.php');
  die;
}

if(!isset($_SESSION['cart'])) {
  header ('Location:shop_coffee.php');
  die;
}

//after payment.php $_POST['card_number'] must exist
//this prevents of displaying this page (invoice.php) if a user types address of this page manually
if(!isset($_SESSION['card_number'])) {
  header ('Location:shop_coffee.php');
  die;
}
else {
  $user = getUser($dbh, $_SESSION['user_id']);//get user info from db
  $user_name = $user['first_name'] . ' ' . $user['last_name'];
  $user_address = $user['street'] . ', ' . $user['city'] . ', ' . $user['province'] . ', ' . $user['country'] . ', ' . $user['postal_code'];
  
  $product = getProduct($dbh, $_SESSION['cart']['product_id']); //get product info
  
  //START inserting purchase info into invoice table
  $query = "INSERT INTO
            invoice
            (
              user_id,
              user_name,
              user_address,
              product_id,
              product_name,
              product_price,
              purchased_quantity,
              gst,
              pst,
              subtotal,
              total
            )
            VALUES
            (
              :user_id,
              :user_name,
              :user_address,
              :product_id,
              :product_name,
              :product_price,
              :purchased_quantity,
              :gst,
              :pst,
              :subtotal,
              :total
            )";
            
  $stmt = $dbh->prepare($query);// prepare query (prepare SQL statement above to be executed)
  
  $stmt->bindValue(':user_id', $user['user_id'], PDO:: PARAM_STR);
  $stmt->bindValue(':user_name', $user_name, PDO:: PARAM_STR);
  $stmt->bindValue(':user_address', $user_address, PDO:: PARAM_STR);
  $stmt->bindValue(':product_id', $product['product_id'], PDO:: PARAM_STR);
  $stmt->bindValue(':product_name', $product['title'], PDO:: PARAM_STR);
  $stmt->bindValue(':product_price', $product['price'], PDO:: PARAM_STR);
  $stmt->bindValue(':purchased_quantity', $_SESSION['cart']['quantity'], PDO:: PARAM_STR);
  $stmt->bindValue(':gst', $_SESSION['gst'], PDO:: PARAM_STR);
  $stmt->bindValue(':pst', $_SESSION['pst'], PDO:: PARAM_STR);
  $stmt->bindValue(':subtotal', $_SESSION['subtotal'], PDO:: PARAM_STR);
  $stmt->bindValue(':total', $_SESSION['total'], PDO:: PARAM_STR);

  if($stmt->execute()) {//if INSERT INTO can be executed
  //END inserting purchase info into invoice table
    
    //START assign invoice_id for INSERTed data
    $id = $dbh->lastInsertId();
    
    $query = "SELECT *
              FROM
              invoice
              WHERE invoice_id = :id";
    
    $stmt = $dbh->prepare($query);
    $stmt->bindValue(':id', $id, PDO::PARAM_INT);
    $stmt->execute();
    $invoice = $stmt->fetch(PDO::FETCH_ASSOC);
    //END assign invoice_id for INSERTed data
    
    //START now minus purchased quantity from available quantity in table product_coffee
    $new_quantity_available = $product['quantity_available'] - $invoice['purchased_quantity'];
    
    $query = "UPDATE product_coffee
              SET quantity_available = :quantity_available
              WHERE product_id = :product_id
             ";
             
    $stmt = $dbh->prepare($query);
    $stmt->bindValue(':quantity_available', $new_quantity_available, PDO:: PARAM_STR);
    $stmt->bindValue(':product_id', $product['product_id'], PDO:: PARAM_STR);
    $stmt->execute();
    //END now minus purchased quantity from available quantity in table product_coffee
    
    
    unset($_SESSION['cart']);
    unset($_SESSION['pst']);
    unset($_SESSION['gst']);
    unset($_SESSION['subtotal']);
    unset($_SESSION['total']);
    unset($_SESSION['card_number']);
    session_regenerate_id();

  }//END if($stmt->execute())

  else{
    die ("can't insert a new invoice");
  }//END else
  
}//END if(!isset($_POST['card_number']))



?>

<!-- <head> -->
<?php include '../includes/head.inc.php' ?>


  <body id="index">

    <div id="wrapper">
    
      <!-- header with navigation -->
      <?php include '../includes/header.inc.php' ?>
      
      <main id="content"> <!-- main content goes here -->
        
        <?php include '../includes/search.inc.php' ?>
        
        <h1><?=$title?></h1>
        
        <h2>Transaction is done!</h2>
        
        <!-- info reflects info on the moment of puchase which is stored in invoice table,
             not current info  from tables product_coffee and users which can change by the time
        -->
        
        <p><strong>Invoice Number:</strong> <?php echo $invoice['invoice_id'] ?></p>
        
        <p><strong>Invoice Date:</strong> <?php echo $invoice['date_of_invoice'] ?></p>
        
        <p>
          <strong>Customer Info:</strong> <br />
          <?php echo $invoice['user_name'] ?><br />
          <?php echo $invoice['user_address'] ?>
        </p>
        
        <table id="invoice_table">
          <tr>
            <th>Product Name</th>
            <th>Price</th>
            <th>Quantity</th>
          </tr>
          
          <tr>
            <td><?php echo $invoice['product_name'] ?></td>
            <td>$<?php echo $invoice['product_price'] ?></td>
            <td><?php echo $invoice['purchased_quantity'] ?></td>
          </tr>
          
          <tr><td colspan="3">&nbsp;</td></tr>
          
          <tr>
            <td colspan="2">Subtotal:</td>
            <td>$<?php echo $invoice['subtotal'] ?></td>
          </tr>
          
          <tr>
            <td colspan="2">PST:</td>
            <td>$<?php echo $invoice['pst'] ?></td>
          </tr>
          
          <tr>
            <td colspan="2">GST:</td>
            <td>$<?php echo $invoice['gst'] ?></td>
          </tr>
          
          <tr>
            <td colspan="2">Total:</td>
            <td>$<?php echo $invoice['total'] ?></td>
          </tr>
          
        </table>
        
        
        
        
      </main> <!-- end main#content -->
      
      <!-- footer -->
      <?php include '../includes/footer.inc.php' ?>
    
    </div> <!-- end #wrapper -->
    
  </body>
  
</html>