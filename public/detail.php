<?php

/**
* Capstone Project PHP
* @file detail.php
* @author Alex Ten
* created_at 2018-09-06
**/


// if no product ID, send back to shop_coffee.php page
if(empty($_GET['product_id'])) {
  header('Location: shop_coffee.php');
  die;
}


require __DIR__ . '/../config.php'; //main config file
require '../functions.php';  //main functions file


$title = 'Product Details';
$active_page = 'detail';

//if recieve session about not enough quantity of product
if(isset($_SESSION['no_quantity'])) {
  $flash_msg = "Sorry, we don't have such quantity";
  unset($_SESSION['no_quantity']);
  session_regenerate_id();
}


//get all info usinge product_id from database to show on this page
$product = getProduct($dbh, $_GET['product_id']);
//var_dump($product); check if get proper info from database

?>

<!-- <head> -->
<?php include '../includes/head.inc.php' ?>


  <body id="index">

    <div id="wrapper">
    
      <!-- header with navigation -->
      <?php include '../includes/header.inc.php' ?>
      
      <main id="content"> <!-- main content goes here -->
        
        <?php include '../includes/search.inc.php' ?>
        
        <?php if(isset($_SESSION['cart'])) { //if anything is added in cart
          include '../includes/cart.inc.php';
        } ?>
        
        <h1><?=$title?></h1><br/>
        
        <?php if(isset($flash_msg)) : ?>
          <h2 id="flash_msg"><?php echo $flash_msg ?></h2>
        <?php endif; ?>
        
        <div class="product_detail">
          <img src="images/coffee_images/<?php echo $product['image']; ?>" alt="<?php echo $product['title']; ?>" />
          <p class="title"><strong><?php echo $product['title']; ?></strong></p><br />
        </div><!-- END div.product_detail -->  
        
        <!-- add to cart code -->
        <form id="add_to_cart_form" action="cart.php" method="post" novalidate="novalidate">
          <input type="hidden"
                 name="product_id" 
                 value="<?php echo $product['product_id'] ?>" />
                 
          <!--<label for="quantity">Quantity</label><br />-->
          <input type="text"
                 id="quantity_field"
                 name="quantity"
                 value="1"
                 placeholder="Quantity" />
                 
          <button id="add_to_cart" type="submit">Add to cart</button>
        </form>
        
        <div id="ul_details">
          <ul>
            <li><strong>Price:</strong> <?php echo $product['price']; ?>$</li>
            <li><strong>Quantity in stock:</strong> <?php echo $product['quantity_available']; ?></li>
            <li><strong>Rating:</strong> <?php echo $product['rating']; ?> star(s)</li>
            <li><strong>Format:</strong> <?php echo $product['category']; ?></li>
            <li><strong>Grade:</strong> <?php echo $product['grade']; ?></li>
          </ul>
        </div><!-- END div#ul_details -->
        
        <div id="description">
          <?php echo $product['description']; ?>
        </div><!--END div#description-->
        
      </main> <!-- end main#content -->
      
      <!-- footer include -->
      <?php include '../includes/footer.inc.php' ?>
    
    </div> <!-- end #wrapper -->
    
  </body>
  
</html>