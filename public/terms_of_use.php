<?php

/**
* Capstone Project PHP
* @file terms_of_use.php
* @author Alex Ten
* created_at 2018-09-05
**/

require __DIR__ . '/../config.php';

$title = 'Terms of Use';

$active_page = 'terms_of_use.php';

?>

<!-- <head> -->
<?php include '../includes/head.inc.php' ?>


  <body id="index">

    <div id="wrapper">
    
      <!-- header with navigation -->
      <?php include '../includes/header.inc.php' ?>
      
      <main id="content"> <!-- main content goes here -->
        
        <?php include '../includes/search.inc.php' ?>
        
        <?php if(isset($_SESSION['cart'])) { //if anything is added in cart
          include '../includes/cart.inc.php';
        } ?>
        
        <h1><?=$title?></h1>
        <br />
        
        <h2>Copyright</h2>
        <p>
          The entire content included in this site, including but not limited to text, 
          graphics or code is copyrighted as a collective work under Canada and other copyright 
          laws, and is the property of Coffeeccino. The collective work includes works 
          that are licensed to Coffeeccino. Copyright 2018, Coffeeccino 
          ALL RIGHTS RESERVED. Permission is granted to electronically copy and print 
          hard copy portions of this site for the sole purpose of placing an order with 
          Coffeeccino or purchasing Coffeeccino products. You may display and, 
          subject to any expressly stated restrictions or limitations relating to specific 
          material, download or print portions of the material from the different areas of 
          the site solely for your own non-commercial use, or to place an order with 
          Coffeeccino or to purchase Coffeeccino products. Any other use, 
          including but not limited to the reproduction, distribution, display or transmission 
          of the content of this site is strictly prohibited, unless authorized by 
          Coffeeccino. You further agree not to change or delete any proprietary notices 
          from materials downloaded from the site.
        </p>
        <br />
        
        <h2>Trademarks</h2>
        <p>
          All trademarks, service marks and trade names of Coffeeccino used in the site 
          are trademarks or registered trademarks of Coffeeccino.
        </p>
        
      </main> <!-- end main#content -->
      
      <!-- footer -->
      <?php include '../includes/footer.inc.php' ?>
    
    </div> <!-- end #wrapper -->
    
  </body>
  
</html>