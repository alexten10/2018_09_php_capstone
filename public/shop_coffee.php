<?php

/**
* Capstone Project PHP
* @file shop_coffee.php
* @author Alex Ten
* created_at 2018-09-05
**/

require __DIR__ . '/../config.php'; //main config file
require '../functions.php';  //main functions file

//var_dump($_SESSION);
$title = 'Shop coffee';
$active_page = 'shop_coffee';


// category list menu
$categories = getCategories($dbh);
//var_dump ($categories); //check if get array with categories


// if get category name
if(!empty($_GET['category'])) {
  if($_GET['category'] == 'All') { //if category name is All
    $products = getAllProducts($dbh);//get all products
    $title = 'All Products: ';
  }//END if($_GET['category'] == 'All')
  else {//if category name is not All
    $products = getProductsByCategory($dbh, $_GET['category']);
    $title = 'Products by Category: ' . $products[0]['category'];
  }
  $category_result_count = count($products);
}//END if(!empty($_GET['category']))

// if get keyword from search field
elseif(!empty($_GET['keyword'])) {
  $keyword = strip_tags(htmlspecialchars($_GET['keyword'])); //assign $_GET['keyword'] value to a variable, after escaping special chars and removing tags for security reasons
  $keyword="%$keyword%"; //search including any chars before typed keyword (%$keyword) any chars after typed ($keyword%)
  //echo $keyword;
  $products = search($dbh, $keyword); //send to the search function: database and value of $keyword
  $title = 'Results for: ' . htmlspecialchars($_GET['keyword']);
  $search_result_count = count($products);//number of items(subarrays)
  //var_dump($products);
}

else {
  $products = getRandomProducts($dbh, 6); //send number 6, which will be used by the function getRandomProducts() to limit number of results to maximium 6
  $title = 'Products you may interested in: ';
}



?>

<!-- <head> -->
<?php include '../includes/head.inc.php' ?>


  <body id="index">
  
    <div id="wrapper">
      
      <!-- header with navigation -->
      <?php include '../includes/header.inc.php' ?>
      
      <main id="content"> <!-- main content goes here -->
        
        <?php include '../includes/search.inc.php' ?>
        
        <?php if(isset($_SESSION['cart'])) { //if anything is added in cart
          include '../includes/cart.inc.php';
        } ?>
        
        <!--/////////////// START categories menu ////////////////////-->
        <div class="categories">
          <h2>Categories</h2>
          
          <ul>
            <?php foreach($categories as $value) : ?>
            <li><a href="shop_coffee.php?category=<?php echo $value['category']?>">
              <?php echo $value['category']?> coffee
              </a></li>
            <?php endforeach; ?>
            <li><a href="shop_coffee.php?category=All">All</a></li>
          </ul>
          
        </div><!-- div.categories -->
        <!--/////////////// END categories menu //////////////////// -->
        
        <h1><?=$title?></h1>
        
        <!-- ///////////// START products list with short description, NOT detailed view ///////////// -->
        <?php if(empty($products)){echo "<h2 id='nothing_found'>Nothing found!</h2>";} ?>
        <?php if(!empty($search_result_count)){ echo '<p><small>You have ' . $search_result_count . " result(s)</small></p>";} ?><!-- message to display number of search results -->
        <?php if(!empty($category_result_count)){ echo '<p><small>Total ' . $category_result_count . " items</small></p>";} ?><!-- message to display number of items in category result -->
        
        <div class="products_list">
        <?php foreach($products as $row) : ?>
          <div class="line"></div>
          <div class="product">
            <div class="img">
              <img src="images/coffee_images/<?php echo $row['image']; ?>" alt="<?php echo $row['title']; ?>" />
            </div><!-- END div.img -->
            
            <div class="details">
              <p class="title"><strong><a class="more" href="detail.php?product_id=<?php echo $row['product_id']; ?>"><?php echo $row['title']; ?></a></strong></p><br />
              <strong>Price:</strong> $<?php echo $row['price']; ?><br />
              <strong>In stock:</strong> <?php echo ($row['in_stock']) ? 'Yes' : 'No'; ?><br />
              <strong>Rating:</strong> <?php echo $row['rating']; ?> star(s)<br />
              <strong>Format:</strong> <?php echo $row['category']; ?>
              <p><a class="more" href="detail.php?product_id=<?php echo $row['product_id']; ?>">See details...</a>
            </div><!-- END div.details -->
          </div><!-- END div.product -->
        
        <?php endforeach; ?>
        </div><!-- END div.products_list -->
        <!-- ///////////// END products list with short description, NOT detailed view ///////////// -->
        
        
        <div id="bottom"></div><!-- this is REQUIRED div. css for that clear:both; otherwise footer will take over the content -->
        
      </main> <!-- end main#content -->
      
      <!-- footer -->
      <?php include '../includes/footer.inc.php' ?>
    
    </div> <!-- end #wrapper -->
    
  </body>
  
</html>


