<?php

/**
* Capstone Project PHP
* @file profile_edit.php
* @author Alex Ten
* created_at 2018-09-13
**/

require __DIR__ . '/../config.php';
require '../functions.php';
use \Classes\Utility\Validator;
$vldtr = new Validator;


$title = 'Profile Edit';
$active_page = 'profile_edit';


//var_dump($_GET['user_id']);
if(empty($_SESSION['logged_in'])) {
  header ('Location: index.php');
  die;
}


if(!empty($_GET['user_id'])) {
  $user_info = getUser($dbh, $_GET['user_id']);
}
//var_dump($user_info);

if($_SERVER['REQUEST_METHOD'] == 'POST') {
  $user_info = $_POST;
  
  $current_date = date('Y-m-d h:m:s');
  
  
  $vldtr->validateForSymbols('first_name');
  $vldtr->required('first_name');
  
  $vldtr->validateForSymbols('last_name');
  $vldtr->required('last_name');
  
  $vldtr->validateStreetSymbols('street');
  $vldtr->required('street');
  
  $vldtr->validateForSymbols('city');
  $vldtr->required('city');
  
  $vldtr->maxLength('postal_code', 12);
  $vldtr->validatePostalCode('postal_code');
  $vldtr->required('postal_code');
  
  $vldtr->validateForSymbols('province');
  $vldtr->required('province');
  
  $vldtr->validateForSymbols('country');
  $vldtr->required('country');
  
  $vldtr->validatePhone('phone');
  $vldtr->required('phone');
  
  $vldtr->validateEmail('email'); //this is 1
  $vldtr->required('email'); //this is 2
  
  // !!! order of calling functions is important !!!
  $vldtr->validatePasswordStrength('password');
  $vldtr->minLength('password', 6);
  $vldtr->maxLength('password', 12);
  $vldtr->required('password');
  $vldtr->passwordsMatch('password', 'password_confirm');
  
  
  
  
  if(empty($vldtr->errors())) {
    
    $query = "UPDATE
              users
              SET
              first_name = :first_name,
              last_name = :last_name,
              street = :street,
              city = :city,
              postal_code = :postal_code,
              province = :province,
              country = :country,
              phone = :phone,
              email = :email,
              password = :password,
              updated_at = :updated_at
              WHERE
              user_id = :user_id
             ";
    $stmt = $dbh->prepare($query);// prepare query (prepare SQL statement above to be executed)
    
    // bind (connecting) values of validated input fields with database values
    $stmt->bindValue(':first_name', $_POST['first_name'], PDO:: PARAM_STR);
    $stmt->bindValue(':last_name', $_POST['last_name'], PDO:: PARAM_STR);
    $stmt->bindValue(':street', $_POST['street'], PDO:: PARAM_STR);
    $stmt->bindValue(':city', $_POST['city'], PDO:: PARAM_STR);
    $stmt->bindValue(':postal_code', $_POST['postal_code'], PDO:: PARAM_STR);
    $stmt->bindValue(':province', $_POST['province'], PDO:: PARAM_STR);
    $stmt->bindValue(':country', $_POST['country'], PDO:: PARAM_STR);
    $stmt->bindValue(':phone', $_POST['phone'], PDO:: PARAM_STR);
    $stmt->bindValue(':email', $_POST['email'], PDO:: PARAM_STR);
    $password_encrypted = password_hash($_POST['password'], PASSWORD_DEFAULT);
    $stmt->bindValue(':password', $password_encrypted, PDO:: PARAM_STR);
    $stmt->bindValue(':updated_at', $current_date, PDO:: PARAM_STR);
    $stmt->bindValue(':user_id', $_POST['user_id'], PDO:: PARAM_STR);
    
    
    if($stmt->execute()) {
      $_SESSION['profile_edited'] = true;
      header('Location: profile.php');
      die;
    }//END if($stmt->execute())
    
    else {//if the query incorrect, set var as a flag
      $user_not_edited = 'Can not edit profile';
    }// END else
    
  }//END if(empty($vldtr->errors()))
  
}//END if($_SERVER['REQUEST_METHOD'] == 'POST')

$errors = $vldtr->errors();
?>

<!-- <head> -->
<?php include '../includes/head.inc.php' ?>


  <body id="index">

    <div id="wrapper">
    
      <!-- header with navigation -->
      <?php include '../includes/header.inc.php' ?>
      
      <main id="content"> <!-- main content goes here -->
        
        <?php include '../includes/search.inc.php' ?>
        
        <?php if(isset($_SESSION['cart'])) { //if anything is added in cart
          include '../includes/cart.inc.php';
        } ?>
        
        <h1><?=$title?></h1>
        <br/>
        
        <?php if(isset($user_not_edited)) :?>
          <h2><?php echo $user_not_edited ?></h2>
        <?php endif; ?>
        
        
        <form method="post"
                action="profile_edit.php"
                id="profile_edit_form"
                name="profile_edit_form"
                accept-charset="utf-8" 
                novalidate="novalidate">
            
          <fieldset>
            <legend><b>Profile Edit</b></legend>
            
            <p>
              <label for="first_name">First Name</label>
              <input type="text"
                     id="first_name"
                     name="first_name"
                     maxlength="255"
                     value="<?php 
                              if(!empty($user_info['first_name'])){
                                echo esc ($user_info['first_name']);}
                            ?>"
                     placeholder="type first name" /><br />
                     <!-- value="<?php...?>" is for sticky input field -->
              
              <!-- display error message near the field -->
              <?php if(!empty($errors['first_name'])) : ?>
                <span class="error"><?=$errors['first_name']?></span><br />
              <?php endif; ?>
            </p>

            <p>
              <label for="last_name">Last Name</label>
              <input type="text"
                     id="last_name"
                     name="last_name"
                     maxlength="255"
                     value="<?php 
                              if(!empty($user_info['last_name'])){
                                echo esc ($user_info['last_name']);}
                            ?>"
                     placeholder="type last name" /><br />
                     <!-- value="<?php//...?>" is for sticky input field -->
              
              <!-- display error message near the field -->
              <?php if(!empty($errors['last_name'])) : ?>
                <span class="error"><?=$errors['last_name']?></span><br />
              <?php endif; ?>
            </p>

            <p>
              <label for="street">Street</label>
              <input type="text"
                     id="street"
                     name="street"
                     maxlength="255"
                     value="<?php 
                              if(!empty($user_info['street'])){
                                echo esc ($user_info['street']);}
                            ?>"
                     placeholder="type street name" /><br />
              
              <!-- display error message near the field -->
              <?php if(!empty($errors['street'])) : ?>
                <span class="error"><?=$errors['street']?></span><br />
              <?php endif; ?>
            </p>

            <p>
              <label for="city">City</label>
              <input type="text"
                     id="city"
                     name="city"
                     maxlength="255"
                     value="<?php 
                              if(!empty($user_info['city'])){
                                echo esc ($user_info['city']);}
                            ?>"
                     placeholder="type city name" /><br />
              
              <!-- display error message near the field -->
              <?php if(!empty($errors['city'])) : ?>
                <span class="error"><?=$errors['city']?></span><br />
              <?php endif; ?>
            </p>

            <p>
              <label for="postal_code">Postal Code</label>
              <input type="text"
                     id="postal_code"
                     name="postal_code"
                     maxlength="255"
                     value="<?php 
                              if(!empty($user_info['postal_code'])){
                                echo esc ($user_info['postal_code']);}
                            ?>"
                     placeholder="type postal_code" /><br />
              
              <!-- display error message near the field -->
              <?php if(!empty($errors['postal_code'])) : ?>
                <span class="error"><?=$errors['postal_code']?></span><br />
              <?php endif; ?>
            </p>

            <p>
              <label for="province">Province</label>
              <input type="text"
                     id="province"
                     name="province"
                     maxlength="255"
                     value="<?php 
                              if(!empty($user_info['province'])){
                                echo esc ($user_info['province']);}
                            ?>"
                     placeholder="Type your province" /><br />
                     
              <?php if(!empty($errors['province'])) : ?>
                <span class="error"><?=$errors['province']?></span><br />
              <?php endif; ?>
            </p>

            <p>
              <label for="country">Country</label>
              <input type="text"
                     id="country"
                     name="country"
                     maxlength="255"
                     value="<?php 
                              if(!empty($user_info['country'])){
                                echo esc ($user_info['country']);}
                            ?>"
                     placeholder="Type your country" /><br />
                     
              <?php if(!empty($errors['country'])) : ?>
                <span class="error"><?=$errors['country']?></span><br />
              <?php endif; ?>
            </p>

            <p>
              <label for="phone">Phone</label>
              <input type="tel"
                     id="phone"
                     name="phone"
                     maxlength="30"
                     value="<?php 
                              if(!empty($user_info['phone'])){
                                echo esc ($user_info['phone']);}
                            ?>"
                     placeholder="(000)222-3344"/><br />
                     
              <?php if(!empty($errors['phone'])) : ?>
                <span class="error"><?=$errors['phone']?></span><br />
              <?php endif; ?>
            </p>

            <p>
              <label for="email">Email Address</label>
              <input type="email"
                     name="email"
                     id="email"
                     value="<?php 
                              if(!empty($user_info['email'])){
                                echo esc ($user_info['email']);}
                            ?>"
                     placeholder="your_email@example.com" /><br />
                     
              <?php if(!empty($errors['email'])) : ?>
                <span class="error"><?=$errors['email']?></span><br />
              <?php endif; ?>
            </p>

            <p>
              <label for="pass">New Password</label>
              <input type="password"
                     id="pass"
                     name="password"
                     maxlength="255"
                     value=""
                     placeholder="Your new password (min 4 characters)" /><br />
                     
              <?php if(!empty($errors['password'])) : ?>
                <span class="error"><?=$errors['password']?></span><br />
              <?php endif; ?>
            </p>

            <p>
              <label for="pass_confirm">Confirm Password</label>
              <input type="password"
                     id="pass_confirm"
                     name="password_confirm"
                     maxlength="255"
                     value=""
                     placeholder="Type again your new password" />
            </p>

            <!-- a hidden field that has product_id value to send with the $_POST array
                 to identify the product to be edited-->
            <input type="hidden" name="user_id" value="<?=$user_info['user_id']?>" />
            
          </fieldset>
        
          <p id="form_submit_buttons">
            <input type="submit"
                   value="Edit"
                   class="button" />
          </p>
        
        </form>
        
        
      </main> <!-- end main#content -->
      
      <!-- footer -->
      <?php include '../includes/footer.inc.php' ?>
    
    </div> <!-- end #wrapper -->
    
  </body>
  
</html>