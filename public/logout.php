<?php

/**
* Capstone Project PHP
* @file logout.php
* @author Alex Ten
* created_at 2018-09-05
**/

require __DIR__ . '/../config.php';

//unsetting all data in SESSION // analog $_SESSION = array();
unset($_SESSION['logged_in']); // logs user out by destroying $_SESSION['logged_in']
unset($_SESSION['user_id']);
// now SESSION array is completely empty
session_regenerate_id(); // regenerate session id
$_SESSION['logged_out'] = true;//set a new key, which is $_SESSION['logged_out'] to be in session.this is used for logout flash message on login.php
header('Location: login.php'); //redirect to the login page



