<?php

namespace Classes\Utility;


class Validator
{
  // our errors array
  private $errors = [];
  
  
  /**
  *validate required fields in $_POST
  *@param String $field_name - name of field to validate
  */
  public function required($field_name)
  {
    $label = ucfirst(str_replace('_', ' ', $field_name));
    if(empty($_POST[$field_name])) {
      $this->errors[$field_name] = "'{$label}' is a required field";
    }//END if
  }//END function required()
  
  
  
  /**
  *validate fields in $_POST for legal characters
  *@param String $field_name - name of field to validate
  */
  public function validateForSymbols($field_name)
  {
    $label = ucfirst(str_replace('_', ' ', $field_name));
    $pattern = '/^[A-z]([A-z\,\.\'\-\s]?)+$/';
    if(preg_match($pattern, $_POST[$field_name], $matches) === 0) { //if no matches found
      $this->errors[$field_name] = "'{$label}' field must contain only legal names in English";
    }//END if
  }//END function validateForSymblols()
  
  
  
  /**
  *validate street field in $_POST for proper format
  *@param String $field_name - name of field to validate
  */
  public function validateStreetSymbols($field_name)
  {
    $label = ucfirst($field_name);//field name ("street") has nothing to replace
    $pattern = '/^[A-z0-9][A-z0-9\,\.\'\-\s]+$/';
    if(preg_match($pattern, $_POST[$field_name], $matches) === 0) { //if no matches found
      $this->errors[$field_name] = "'{$label}' field must contain only legal street number and names in English";
    }//END if
  }//END function validateStreetSymbols()
  
  
  
  /**
  *validate postal_code field in $_POST for Canadian format
  *@param String $field_name - name of field to validate
  */
  public function validatePostalCode($field_name)
  {
    $label = ucfirst(str_replace('_', ' ', $field_name));
    $pattern = '/^[A-z][0-9][A-z]\s?[0-9][A-z][0-9]$/';
    if(preg_match($pattern, $_POST[$field_name], $matches) === 0) { //if no matches found
      $this->errors[$field_name] = "'{$label}' field must contain valid Canadian postal code, eg. 'A1A 1A1'";
    }//END if
  }//END function validatePostalCode()
  
  
  
  /**
  *validate phone field in $_POST for North American format
  *@param String $field_name - name of field to validate
  */
  public function validatePhone($field_name)
  {
    $label = ucfirst(str_replace('_', ' ', $field_name));
    $pattern = '/^\s?\+?\s?[0-9]?[\-\.\s]?\(?([0-9]{3})\)?[\-\.\s]?([0-9]{3})[\-\.\s]?([0-9]{4})\s?$/';
    if(preg_match($pattern, $_POST[$field_name], $matches) === 0) { //if no matches found
      $this->errors[$field_name] = "'{$label}' field must contain valid phone number, eg. '+1 204 111-1111'";
    }//END if
  }//END function validatePhone()
  
  
  
  /**
  *validate email field in $_POST for proper format
  *@param String $field_name - name of field to validate
  */
  public function validateEmail($field_name)
  {
    if(!filter_var(($_POST[$field_name]), FILTER_VALIDATE_EMAIL)) {
      $this->errors[$field_name] = "Please enter a valid email address";
    }//END if
  }//END function validateEmail()
  
  
  
  /**
  *validate password field in $_POST for maximum length
  *@param String $field_name - name of field to validate
  *@param int $max_length - number of max length
  */
  function maxLength($field_name, $max_length)
  {
    $label = ucfirst(str_replace('_', ' ', $field_name));
    if(strlen($_POST[$field_name]) > $max_length) {
      $this->errors[$field_name] = "'{$label}' field must be at maximum {$max_length} characters long";
    }//END if
  }//END function maxLength()
    
  
  
  /**
  *validate password field in $_POST for minimum length
  *@param String $field_name - name of field to validate
  *@param int $min_length - number of min length
  */
  function minLength($field_name, $min_length)
  {
    $label = ucfirst(str_replace('_', ' ', $field_name));
    if(strlen($_POST[$field_name]) < $min_length) {
      $this->errors[$field_name] = "'{$label}' field must be at least {$min_length} characters long";
    }//END if
  }//END function minLength()
    
  
  
  /**
  *validate password field in $_POST for password strength 
  *@param String $field_name - name of field to validate
  */
  public function validatePasswordStrength($field_name)
  {
    $label = ucfirst($field_name);
    //no need to state the min lenth and max length {6,12} of password, as it has been prevously checked
    // < and > are excluded form allowed special characters
    $pattern = '/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[\!\@\#\$\%\^\&\*\(\)\-\_\=\+\/\\\|\}\{\[\]\'\"\:\;\?\.\,\`\~])/'; 
    if(preg_match($pattern, $_POST[$field_name], $matches) === 0) { //if no matches found
      $this->errors[$field_name] = "'{$label}' field must contain at least 1 upper case letter, 1 lower case letter,
                                    1 digit, 1 spcial character (except <strong><</strong> and <strong>></strong>)";
    }//END if
  }//END function validatePasswordStrength()
  
  
  
  /**
  *validate password and password_confirm fields in $_POST to match
  *@param String $field_name - password field
  *@param string $compare_field - password_confirm field
  */
  public function passwordsMatch($field_name, $compare_field)
  {
    if($_POST[$field_name] != $_POST[$compare_field]) {
      $this->errors[$field_name] = "Passwords do not match! Please enter again.";
    }//END if
  }//END function passwordsMatch()
  
  
  
  
  
  
  
  /**
  *validate credit_card field in $_POST for 16 digits
  *@param INT $field_name - name of field to validate
  */
  public function validateCreditCard($field_name)
  {
    $label = ucfirst(str_replace('_', ' ', $field_name));
    $pattern = '/^[0-9]{16}$/';
    if(preg_match($pattern, $_POST[$field_name], $matches) === 0) { //if no matches found
      $this->errors[$field_name] = "'{$label}' field must have 16 digit card number without spaces";
    }//END if
  }//END function validatePostalCode()
  
  
  
  /**
  *validate Card Expiry field in $_POST for 4 digits
  *@param INT $field_name - name of field to validate
  */
  public function validateCardExpiry($field_name)
  {
    $label = ucfirst(str_replace('_', ' ', $field_name));
    $pattern = '/^[0-9]{4}$/';
    if(preg_match($pattern, $_POST[$field_name], $matches) === 0) { //if no matches found
      $this->errors[$field_name] = "'{$label}' field must have 4 digit card expiry date without spaces";
    }//END if
  }//END function validatePostalCode()
  
  
  /**
  *validate CVV (credit card cvv) field in $_POST for 3 digits
  *@param INT $field_name - name of field to validate
  */
  public function validateCvv($field_name)
  {
    $label = ucfirst(str_replace('_', ' ', $field_name));
    $pattern = '/^[0-9]{3}$/';
    if(preg_match($pattern, $_POST[$field_name], $matches) === 0) { //if no matches found
      $this->errors[$field_name] = "'{$label}' field must have 3 digit card CVV";
    }//END if
  }//END function validatePostalCode()
  
  
  
  /**
  *@return Array errors - array of error messages
  */
  public function errors()
  {
    return $this->errors;
  }//END function errors()
  
  
  
}//END class Validator


























