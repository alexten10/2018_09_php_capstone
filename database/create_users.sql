DROP DATABASE IF EXISTS coffeeccino_db;



CREATE DATABASE coffeeccino_db;
#GRANT ALL ON coffeeccino_db.* to 'admin'@'localhost' IDENTIFIED BY 'mypass'; // this is if you want set access by user name and password
USE coffeeccino_db;





DROP TABLE if exists users;
CREATE TABLE users (
              user_id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
              first_name VARCHAR (255),
              last_name VARCHAR (255),
              street VARCHAR (255),
              city VARCHAR (255),
              postal_code VARCHAR (255),
              province VARCHAR (255),
              country VARCHAR (255),
              phone VARCHAR (30),
              email VARCHAR (255),
              password VARCHAR (255),
              created_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
              updated_at DATETIME DEFAULT NULL
             );

INSERT INTO users (user_id, first_name, last_name, street, city, postal_code, province, country, phone, email, password)
VALUES 
      (
       1,
       'Napoleon',
       'Bonaparte',
       'Main',
       'Winnipeg',
       'Q1W1E1',
       'MB',
       'Canada',
       2041112233,
       'user1@gmail.com',
       '$2y$10$9Ld1vC0ajolHXPuOpdEER.NzrMtA85s/CyznZCYtTeBv/BtGRLhlq' # qqqqQ!1
      );






DROP TABLE IF EXISTS product_coffee;
CREATE TABLE product_coffee (
               product_id INT (11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
               title VARCHAR (255),
               image VARCHAR (255),
               price DECIMAL (5,2),
               description TEXT,
               category VARCHAR (255),
               in_stock BOOLEAN,
               quantity_available INT (4),
               supplier VARCHAR (255),
               grade CHAR (1),
               rating INT (1),
               deleted BOOLEAN NOT NULL DEFAULT FALSE,
               created_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
               updated_at DATETIME DEFAULT NULL
             );

INSERT INTO product_coffee (product_id, title, image, price, description, category, in_stock, quantity_available, supplier, grade, rating, deleted)
VALUES (1, 'Coarse Grind Bag', 'coffee_bag_coarse_grind.jpg', 1.99, 'The signature flavour in every cup of Tim Hortons Original Blend Coffee is created using our own unique blend of 100% Arabica beans. Selected from the world\'s most renowned coffee growing regions, our beans are blended and roasted with care to deliver the consistent taste of Tim Hortons every time. Coarse Grind is ideal for use in French Press coffee makers.', 'Bag', 1, 3, 'coffee ltd', 'A', 1, 0),
       (2, 'Colombian Bag', 'coffee_bag_colombian.jpg', 2.98, 'Tim Hortons Colombian Coffee is made with 100% Arabica beans grown in the rich volcanic soils of Colombia, where the tropical climate and geography is ideal for growing coffee. This premium coffee is roasted with care to deliver a well-balanced, crisp flavour and smooth finish.', 'Bag', 1, 14, 'bean ltd', 'B', 2, 0),
       (3, 'Dark Roast Bag', 'coffee_bag_dark_roast.jpg', 3.97, 'Tim Hortons Dark Roast Coffee is made with 100% Arabica beans selected from the world\'s most renowned coffee growing regions. This premium blend is roasted with care to deliver a rich and full flavoured dark roast coffee, with a smooth finish.', 'Bag', 1, 25, 'roast ltd', 'C', 3, 0),
       (4, 'Decaf Bag', 'coffee_bag_decaf.jpg', 4.96, '100% Arabica beans, decaffeinated by the Swiss Water process to preserve our signature flavour.', 'Bag', 0, 0, 'aroma ltd', 'A', 4, 0),
       (5, 'French Roast Bag', 'coffee_bag_french_roast.jpg', 5.95, 'Tim Hortons French Roast Coffee is expertly roasted to deliver a bolder, stronger, and more full-bodied flavour. Made with 100% Arabica beans selected from the world\'s most renowned coffee growing regions, this premium coffee is our darkest blend with a hint of smokiness in every cup.', 'Bag', 1, 44, 'coffee ltd', 'B', 5, 0),
       (6, 'French Vanilla Bag', 'coffee_bag_french_vanilla.jpg', 6.94, 'Tim Hortons French Vanilla Coffee is created from our own unique blend of 100% Arabica beans and a hint of French Vanilla sweetness. This premium coffee is blended and roasted with care to deliver the consistent taste of Tim Hortons with a balanced sweetness and smooth finish every time.', 'Bag', 1, 58, 'bean ltd', 'C', 1, 0),
       (7, 'Hazelnut Bag', 'coffee_bag_hazelnut.jpg', 7.93, 'Tim Hortons Hazelnut Coffee is created from our own unique blend of 100% Arabica beans and a hint of buttery hazelnut flavour. This premium coffee is blended and roasted with care to deliver the consistent taste of Tim Hortons with a balanced sweetness and smooth finish every time.', 'Bag', 1, 66, 'roast ltd', 'A', 2, 0),
       (8, 'Original Bag', 'coffee_bag_original.jpg', 8.92, 'The signature flavour in every cup of Tim Hortons Original Blend Coffee is created using our own unique blend of 100% Arabica beans. Selected from the world\'s most renowned coffee growing regions, our beans are blended and roasted with care to deliver the consistent taste of Tim Hortons every time.', 'Bag', 1, 75, 'aroma ltd', 'B', 3, 0),
       (9, 'Whole Bean Bag', 'coffee_bag_whole_bean.jpg', 9.91, 'The signature flavour in every cup of Tim Hortons Original Blend Coffee is created using our own unique blend of 100% Arabica beans. Selected from the world\'s most renowned coffee growing regions, our beans are blended and roasted with care to deliver the consistent taste of Tim Hortons every time.', 'Bag', 1, 86, 'coffee ltd', 'C', 4, 0),
       (10, 'Dark Roast Canister', 'coffee_canister_dark_roast.jpg', 10.90, 'Tim Hortons Dark Roast Coffee is made with 100% Arabica beans selected from the world\'s most renowned coffee growing regions. This premium blend coffee is roasted with care to deliver a rich and full flavoured dark roast coffee, with a smooth finish.', 'Canister', 1, 92, 'bean ltd', 'A', 5, 0),
       (11, 'Original Canister', 'coffee_canister_original.jpg', 11.89, 'The signature flavour in every cup of Tim Hortons Original Blend Coffee is created using our own unique blend of 100% Arabica beans. Selected from the world\'s most renowned coffee growing regions, our beans are blended and roasted with care to deliver the consistent taste of Tim Hortons every time.', 'Canister', 1, 100, 'roast ltd', 'B', 1, 0),
       (12, 'Colombian K-cup', 'coffee_kcup_colombian.jpg', 12.88, 'Tim Hortons Colombian Coffee is made with 100% Arabica beans grown in the rich volcanic soils of Colombia, where the tropical climate and geography is ideal for growing coffee. This premium coffee is roasted with care to deliver a well-balanced, crisp flavour and smooth finish.', 'K-cup', 1, 112, 'aroma ltd', 'C', 2, 0),
       (13, 'Dark Roast K-cup', 'coffee_kcup_dark_roast_large.jpg', 13.87, 'Tim Hortons Dark Roast Coffee is made with 100% Arabica beans selected from the world\'s most renowned coffee growing regions. This premium blend is roasted with care to deliver a rich and full flavoured dark roast coffee, with a smooth finish.', 'K-cup', 1, 123, 'coffee ltd', 'A', 3, 0),
       (14, 'Dark Roast Small K-cup', 'coffee_kcup_dark_roast_small.jpg', 14.86, 'Tim Hortons Dark Roast Coffee is made with 100% Arabica beans selected from the world\'s most renowned coffee growing regions. This premium blend is roasted with care to deliver a rich and full flavoured dark roast coffee, with a smooth finish.', 'K-cup', 1, 139, 'bean ltd', 'B', 4, 0),
       (15, 'Decaf K-cup', 'coffee_kcup_decaf.jpg', 15.85, '100% Arabica beans, decaffeinated by the Swiss Water process to preserve our signature flavour. For use with Keurig', 'K-cup', 1, 145, 'roast ltd', 'C', 5, 0),
       (16, 'French Roast K-cup', 'coffee_kcup_french_roast.jpg', 16.84, 'Tim Hortons French Roast Coffee is expertly roasted to deliver a bolder, stronger, and more full-bodied flavour. Made with 100% Arabica beans selected from the world\'s most renowned coffee growing regions, this premium coffee is our darkest blend with a hint of smokiness in every cup.', 'K-cup', 1, 159, 'aroma ltd', 'A', 1, 0),
       (17, 'French Vanilla K-cup', 'coffee_kcup_french_vanilla_large.jpg', 17.83, 'Tim Hortons French Vanilla Coffee is created from our own unique blend of 100% Arabica beans and a hint of French Vanilla sweetness. This premium coffee is blended and roasted with care to deliver the consistent taste of Tim Hortons with a balanced sweetness and smooth finish every time.', 'K-cup', 1, 164, 'coffee ltd', 'B', 2, 0),
       (18, 'French Vanilla Small k-cup', 'coffee_kcup_french_vanilla_small.jpg', 18.82, 'Sweet and creamy French Vanilla and coffee flavours combine for the ultimate comfort in a cup. Get the same Tim Hortons taste you love at home.', 'K-cup', 1, 177, 'bean ltd', 'C', 3, 0),
       (19, 'Hazelnut K-cup', 'coffee_kcup_hazelnut.jpg', 19.81, 'Tim Hortons Hazelnut Coffee is created from our own unique blend of 100% Arabica beans and a hint of buttery hazelnut flavour. This premium coffee is blended and roasted with care to deliver the consistent taste of Tim Hortons with a balanced sweetness and smooth finish every time.', 'K-cup', 1, 183, 'roast ltd', 'A', 4, 0),
       (20, 'Original K-cup', 'coffee_kcup_original_large.jpg', 20.79, 'The signature flavour in every cup of Tim Hortons Original Blend Coffee is created using our own unique blend of 100% Arabica beans. Selected from the world\'s most renowned coffee growing regions, our beans are blended and roasted with care to deliver the consistent taste of Tim Hortons every time.', 'K-cup', 1, 192, 'aroma ltd', 'B', 5, 0),
       (21, 'Original Small K-cup', 'coffee_kcup_original_small.jpg', 21.78, 'The signature flavour in every cup of Tim Hortons Original Blend Coffee is created using our own unique blend of 100% Arabica beans. Selected from the world\'s most renowned coffee growing regions, our beans are blended and roasted with care to deliver the consistent taste of Tim Hortons every time.', 'K-cup', 1, 203, 'coffee ltd', 'C', 1, 0),
       (22, 'Variety Pack K-cup', 'coffee_kcup_variety_pack.jpg', 22.77, 'Enjoy this convenient variety pack with our premium Original, Dark Roast, and Decaf blends of 100% Arabica coffee beans. For use with Keurig home brewing systems.', 'K-cup', 0, 0, 'bean ltd', 'A', 2, 1);







DROP TABLE IF EXISTS invoice;
CREATE TABLE invoice (
               invoice_id INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
               user_id INT NOT NULL,
               user_name VARCHAR (255),
               user_address VARCHAR (255),
               product_id INT NOT NULL,
               product_name VARCHAR (255),
               product_price DECIMAL (5,2),
               purchased_quantity INT (4),
               gst DECIMAL (5,2),
               pst DECIMAL (5,2),
               subtotal DECIMAL (11,2),
               total DECIMAL (5,2),
               date_of_invoice DATE NOT NULL DEFAULT CURRENT_TIMESTAMP
             );






DROP TABLE if exists admins;
CREATE TABLE admins (
              admin_id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
              first_name VARCHAR (255),
              last_name VARCHAR (255),
              email VARCHAR (255),
              password VARCHAR (255)
             );

INSERT INTO 
admins
 (
   first_name,
   last_name,
   email,
   password
  )
VALUES
  (
    'James',
    'Bond',
    'admin@gmail.com',
    '$2y$10$Z2Gh.2YNlA73A4aQek7Qk.SWcTsiek0ctOgrUrrDXrZYuxons2Vl2' # zzzzZ!1
  );

#zzzzZ!1





